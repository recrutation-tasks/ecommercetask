using Ecommerce.Store.API.Domain.Queries;
using JetBrains.Annotations;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Store.API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CommoditiesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CommoditiesController([NotNull] IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [Route("BasicInfo")]
        [HttpGet]
        public async Task<IActionResult> Commodities(CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetCommoditiesBasicInfoQuery()));
        }
    }
}