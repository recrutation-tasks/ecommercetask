﻿using Ecommerce.DataTransfer.Model;
using MediatR;

namespace Ecommerce.Store.API.Domain.Queries
{
    public class GetCommoditiesBasicInfoQuery : IRequest<IReadOnlyCollection<CommodityBasicInfo>>
    {
    }
}
