﻿using AutoMapper;
using Ecommerce.DataTransfer.Model;
using Ecommerce.Store.API.Domain.Contexts;
using JetBrains.Annotations;
using MediatR;

namespace Ecommerce.Store.API.Domain.Queries
{
    public class GetCommoditiesBasicInfoQueryHadler : IRequestHandler<GetCommoditiesBasicInfoQuery, IReadOnlyCollection<CommodityBasicInfo>>
    {
        private readonly IMapper _mapper;
        private readonly IDomainContext _context;

        public GetCommoditiesBasicInfoQueryHadler([NotNull] IDomainContext context, [NotNull] IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IReadOnlyCollection<CommodityBasicInfo>> Handle([NotNull] GetCommoditiesBasicInfoQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            if (cancellationToken.IsCancellationRequested)
                throw new OperationCanceledException(cancellationToken);

            var commodities = _context.GetAll();

            return Task.FromResult(_mapper.Map<IEnumerable<Commodity>, IEnumerable<CommodityBasicInfo>>(commodities).ToList() as IReadOnlyCollection<CommodityBasicInfo>);
        }
    }
}
