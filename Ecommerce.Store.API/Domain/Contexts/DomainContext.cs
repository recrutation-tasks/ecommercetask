﻿using Ecommerce.Database;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Store.API.Domain.Contexts
{
    public class DomainContext : IDomainContext
    {
        private readonly IEcommerceStorageContext _storageContext;

        public DomainContext([NotNull] IEcommerceStorageContext storageContext)
            => _storageContext = storageContext ?? throw new ArgumentNullException(nameof(storageContext));

        public IReadOnlyCollection<Commodity> GetAll()
        {
            var dataModels = _storageContext.Get<CommodityDM>().Include(c => c.Quantity).ToList();

            return dataModels.Select(x => new Commodity(x.Id, x.Name, x.Description,
                new Quantity(x.Id, x.Quantity.Value, x.Quantity.UnitName), x.PricePerUnit))
                .ToList();
        }
    }
}
