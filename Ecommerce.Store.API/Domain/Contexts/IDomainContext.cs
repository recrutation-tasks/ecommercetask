﻿namespace Ecommerce.Store.API.Domain.Contexts
{
    public interface IDomainContext
    {
        IReadOnlyCollection<Commodity> GetAll();
    }
}
