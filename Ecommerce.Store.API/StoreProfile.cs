﻿using AutoMapper;
using Ecommerce.DataTransfer.Model;

namespace Ecommerce.Store.API
{
    public class StoreProfile : Profile
    {
        public StoreProfile()
        {
            CreateMap<Commodity, CommodityBasicInfo>()
                .ForMember(c => c.CurrencySymbol, opt => opt.MapFrom(x => "$"))
                .ForMember(c => c.UnitName, opt => opt.MapFrom(x => x.Quantity.UnitName));
        }
    }
}
