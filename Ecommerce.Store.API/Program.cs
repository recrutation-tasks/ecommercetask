using Ecommerce.Database;
using Ecommerce.Store.API;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Ecommerce.Api.Common;
using Ecommerce.Store.API.Domain.Contexts;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDefaultApiVersioning();
builder.Services.AddMediatR(typeof(Program).Assembly);
builder.Services.AddAutoMapper(typeof(StoreProfile));
builder.Services.AddDbContext<IEcommerceStorageContext, EcommerceContext>(config => config.UseSqlServer(Metadata.ConnectionString));
builder.Services.AddHealthChecks().AddCheck<BasicHealthCheckAsync>("Basic").AddDbContextCheck<EcommerceContext>();
builder.Services.AddScoped<IDomainContext, DomainContext>();

var app = builder.Build();

app.UseHttpLogging();

app.MapHealthChecks("/health");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
