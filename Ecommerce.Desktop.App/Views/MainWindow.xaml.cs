﻿using System.Threading;
using Ecommerce.Desktop.Client.ViewModels;
using System.Threading.Tasks;
using System.Windows;

namespace Ecommerce.Desktop.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(MainWindowViewModel viewModel)
        {
            InitializeComponent();
            WindowState = WindowState.Maximized;
            DataContext = viewModel;
        }
    }
}
