﻿using AutoMapper;
using Ecommerce.DataTransfer.Model;
using Ecommerce.Desktop.Client.ViewModels.Basket;
using Ecommerce.Desktop.Client.ViewModels.Commodity;
using Ecommerce.Desktop.Client.ViewModels.SpecialOffers;

namespace Ecommerce.Desktop.Client.Mapping
{
    public class EcommerceAppMapper : IEcommerceAppMapper
    {
        private readonly IMapper _mapper;

        public EcommerceAppMapper()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CommodityBasicInfo, CommodityListItemViewModel>();
                cfg.CreateMap<SpecialOfferBasicInfo, SpecialOfferListItemViewModel>();
                cfg.CreateMap<BasketContent, BasketViewModel>();
                cfg.CreateMap<CommodityOrder, BasketListItemViewModel>();
            }).CreateMapper();
        }

        public IMapper Mapper => _mapper;
    }
}
