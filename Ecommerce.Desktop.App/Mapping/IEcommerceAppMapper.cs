﻿using AutoMapper;

namespace Ecommerce.Desktop.Client.Mapping
{
    public interface IEcommerceAppMapper
    {
        IMapper Mapper { get; }
    }
}
