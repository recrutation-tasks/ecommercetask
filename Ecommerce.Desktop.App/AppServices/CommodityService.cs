﻿using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public class CommodityService : RestServiceInterfaceBase, ICommodityService
    {
        public CommodityService([NotNull] IServicesSettings servicesSettings)
            : base(servicesSettings.CommodityServiceHostUrl, servicesSettings.CommodityServiceVersion, servicesSettings.CommodityServiceRetriesLimit)
        { }

        public async Task<IReadOnlyCollection<CommodityBasicInfo>> GetBasicInfosAsync(CancellationToken cancellationToken)
            => await ResilentExecuteOnServiceAsync<IReadOnlyCollection<CommodityBasicInfo>>("/Commodities/BasicInfo", Method.Get, cancellationToken);
    }
}
