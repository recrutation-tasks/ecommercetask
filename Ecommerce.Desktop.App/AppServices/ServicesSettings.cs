﻿using JetBrains.Annotations;
using System;
using System.Configuration;

namespace Ecommerce.Desktop.Client.AppServices
{
    public class ServicesSettings : IServicesSettings
    {
#pragma warning disable CS8604 // Possible null reference argument.
        public Uri CommodityServiceHostUrl => GetUriOrThrow(ConfigurationManager.AppSettings.Get(nameof(CommodityServiceHostUrl)));
        public string CommodityServiceVersion => GetTextOrThrow(ConfigurationManager.AppSettings.Get(nameof(CommodityServiceVersion)));
        public int CommodityServiceRetriesLimit => GetIntOrThrow(ConfigurationManager.AppSettings.Get(nameof(CommodityServiceRetriesLimit)));

        public Uri SpecialOfferServiceHostUrl => GetUriOrThrow(ConfigurationManager.AppSettings.Get(nameof(SpecialOfferServiceHostUrl)));
        public string SpecialOfferServiceVersion => GetTextOrThrow(ConfigurationManager.AppSettings.Get(nameof(SpecialOfferServiceVersion)));
        public int SpecialOfferServiceRetriesLimit => GetIntOrThrow(ConfigurationManager.AppSettings.Get(nameof(SpecialOfferServiceRetriesLimit)));

        public Uri BasketServiceHostUrl => GetUriOrThrow(ConfigurationManager.AppSettings.Get(nameof(BasketServiceHostUrl)));
        public string BasketServiceVersion => GetTextOrThrow(ConfigurationManager.AppSettings.Get(nameof(BasketServiceVersion)));
        public int BasketServiceRetriesLimit => GetIntOrThrow(ConfigurationManager.AppSettings.Get(nameof(BasketServiceRetriesLimit)));
#pragma warning restore CS8604 // Possible null reference argument.

        private Uri GetUriOrThrow([CanBeNull] string uriString)
        {
            var created = Uri.TryCreate(uriString, default, out Uri? result);
            if (created && result != null)
                return result;

            throw new ConfigurationErrorsException($"Url configuration {uriString} is invalid");
        }

        private int GetIntOrThrow([CanBeNull] string retriesString)
        {
            var parsed = Int32.TryParse(retriesString, out int result);
            if (parsed)
                return result;

            throw new ConfigurationErrorsException($"Retries configuration {retriesString} is invalid");
        }

        private string GetTextOrThrow(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                throw new ConfigurationErrorsException($"Configuration for parameter {text} is invalid");

            return text;
        }

    }
}
