﻿using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public class SpecialOffersService : RestServiceInterfaceBase, ISpecialOffersService
    {
        public SpecialOffersService([NotNull] IServicesSettings servicesSettings)
            : base(servicesSettings.SpecialOfferServiceHostUrl, servicesSettings.SpecialOfferServiceVersion, servicesSettings.SpecialOfferServiceRetriesLimit) 
        { }

        public async Task<IReadOnlyCollection<SpecialOfferBasicInfo>> GetBasicInfosAsync(CancellationToken cancellationToken)
            => await ResilentExecuteOnServiceAsync<IReadOnlyCollection<SpecialOfferBasicInfo>>("/SpecialOffers/BasicInfo", Method.Get, cancellationToken);
    }
}
