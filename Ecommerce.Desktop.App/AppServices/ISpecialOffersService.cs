﻿using Ecommerce.DataTransfer.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public interface ISpecialOffersService
    {
        Task<IReadOnlyCollection<SpecialOfferBasicInfo>> GetBasicInfosAsync(CancellationToken cancellationToken);
    }
}
