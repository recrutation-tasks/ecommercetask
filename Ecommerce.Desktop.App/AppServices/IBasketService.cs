﻿using Ecommerce.DataTransfer.Model;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public interface IBasketService
    {
        Task<BasketContent> GetBasketContent(int customerId, CancellationToken cancellationToken);
        Task<BasketContent> AddToBasket(int customerId, int commodityId, decimal quantity, CancellationToken cancellationToken);
        Task<BasketContent> RemoveFromBasket(int customerId, int commodityId, decimal quantity, CancellationToken cancellationToken);
    }
}
