﻿using Ecommerce.DataTransfer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public interface ICommodityService
    {
        Task<IReadOnlyCollection<CommodityBasicInfo>> GetBasicInfosAsync(CancellationToken cancellationToken);
    }
}
