﻿using System;

namespace Ecommerce.Desktop.Client.AppServices
{
    public interface IServicesSettings
    {
        Uri CommodityServiceHostUrl { get; }
        string CommodityServiceVersion { get; }
        int CommodityServiceRetriesLimit { get; }

        Uri SpecialOfferServiceHostUrl { get; }
        string SpecialOfferServiceVersion { get; }
        int SpecialOfferServiceRetriesLimit { get; }

        Uri BasketServiceHostUrl { get; }
        string BasketServiceVersion { get; }
        int BasketServiceRetriesLimit { get; }
    }
}
