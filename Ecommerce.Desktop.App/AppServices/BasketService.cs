﻿using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using RestSharp;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public class BasketService : RestServiceInterfaceBase, IBasketService
    {
        public BasketService([NotNull] IServicesSettings servicesSettings)
            : base(servicesSettings.BasketServiceHostUrl, servicesSettings.BasketServiceVersion, servicesSettings.BasketServiceRetriesLimit) 
        { }

        public async Task<BasketContent> AddToBasket(int customerId, int commodityId, decimal quantity, CancellationToken cancellationToken)
            => await ResilentExecuteOnServiceAsync<BasketContent>($"/Basket/AddToBasket/{customerId}/{commodityId}/{quantity}", Method.Patch, cancellationToken);

        public async Task<BasketContent> RemoveFromBasket(int customerId, int commodityId, decimal quantity, CancellationToken cancellationToken)
            => await ResilentExecuteOnServiceAsync<BasketContent>($"/Basket/RemoveFromBasket/{customerId}/{commodityId}/{quantity}", Method.Patch, cancellationToken);

        public async Task<BasketContent> GetBasketContent(int customerId, CancellationToken cancellationToken)
            => await ResilentExecuteOnServiceAsync<BasketContent>($"/Basket/BasketContent/{customerId}", Method.Get, cancellationToken);
    }
}
