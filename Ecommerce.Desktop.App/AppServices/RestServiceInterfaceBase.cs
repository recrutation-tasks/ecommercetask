﻿using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using RestSharp;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.AppServices
{
    public abstract class RestServiceInterfaceBase
    {
        private const string HealtyResponse = "Healthy";
        private const string HealthcheckPath = "health";
        private const int RetryDelay = 500;
        protected readonly RestClient _client;
        private readonly string _version;

        private readonly AsyncRetryPolicy<bool> _healthcheckRetryPolicy;
        private readonly AsyncRetryPolicy _resourceRetryPolicy;

        protected RestServiceInterfaceBase([NotNull] Uri hostUrl, string version, int retries)
        {
            if (hostUrl is null)
                throw new ArgumentNullException(nameof(hostUrl));
            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentNullException(nameof(version));

            _version = version;

            _client = new RestClient(hostUrl);
            _client.UseJson();

            _healthcheckRetryPolicy = Policy.Handle<HttpRequestException>()
                .OrResult<bool>(result => !result)
                .WaitAndRetryAsync(retries, count => TimeSpan.FromMilliseconds(count * RetryDelay));

            _resourceRetryPolicy = Policy.Handle<HttpRequestException>().WaitAndRetryAsync(retries, count => TimeSpan.FromMilliseconds(RetryDelay));
        }

        protected async Task<T?> ResilentExecuteOnServiceAsync<T>(string resource, Method method, CancellationToken token) where T : class
        {
            await CheckHealth(token);

            return await TryGetResource<T>(resource, method, token);
        }

        private async Task<T?> TryGetResource<T>(string resource, Method method, CancellationToken token) where T : class
        {
            var resourceRequest = new RestRequest("/api/" + _version + resource, method);
            T finalResult = null;

            try
            {
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                finalResult = await _resourceRetryPolicy.ExecuteAsync(async (t) =>
                {
                    var result = await _client.ExecuteAsync<T>(resourceRequest, token).ConfigureAwait(false);
                    return result?.Data;
                }, token);
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
            }
            catch (OperationCanceledException) { }

            if (finalResult is null && !token.IsCancellationRequested)
                throw new Exception($"Service query failed at: {_client.BuildUri(resourceRequest)}");

            return finalResult;
        }

        private async Task CheckHealth(CancellationToken cancellationToken)
        {
            var healthCheck = new RestRequest(HealthcheckPath, Method.Get);

            bool healthy = false;
            try
            {
                healthy = await _healthcheckRetryPolicy.ExecuteAsync(async (t) =>
                {
                    var response = await _client.ExecuteAsync(healthCheck, t);
                    if (response is null)
                        return false;

                    return response?.Content == HealtyResponse;
                }, cancellationToken);
            }
            catch (OperationCanceledException) { }

            if (!healthy && !cancellationToken.IsCancellationRequested)
                throw new Exception($"Service health check failed at: {_client.BuildUri(healthCheck)}");
        }
    }
}
