﻿namespace Ecommerce.Desktop.Client.ViewModels.Commodity
{
    public class CommodityListItemViewModel
    {
        public int Id { get; private set; }
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public string Name { get; private set; }
        public string Description { get; private set; }
        public decimal PricePerUnit { get; private set; }
        public string UnitName { get; private set; }
        public string CurrencySymbol { get; private set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    }
}
