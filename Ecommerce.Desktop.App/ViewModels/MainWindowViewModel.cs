﻿using Ecommerce.DataTransfer.Model;
using Ecommerce.Desktop.Client.AppServices;
using Ecommerce.Desktop.Client.Controls.Services.BusyService;
using Ecommerce.Desktop.Client.Mapping;
using Ecommerce.Desktop.Client.ViewModels.Basket;
using Ecommerce.Desktop.Client.ViewModels.Commodity;
using Ecommerce.Desktop.Client.ViewModels.SpecialOffers;
using JetBrains.Annotations;
using Microsoft.Xaml.Behaviors.Core;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ecommerce.Desktop.Client.ViewModels
{
    public class MainWindowViewModel : Prism.Mvvm.BindableBase
    {
        private readonly ICommodityService _commodityService;
        private readonly ISpecialOffersService _specialOffersService;
        private readonly IBasketService _basketService;
        private readonly IEcommerceAppMapper _mapper;
        private readonly IBusyService _busyService;

        public MainWindowViewModel(
            [NotNull] ICommodityService commodityService, 
            [NotNull] ISpecialOffersService specialOffersService, 
            [NotNull] IBasketService basketService, 
            [NotNull] IEcommerceAppMapper mapper,
            [NotNull] IBusyService busyService)
        {
            _commodityService = commodityService ?? throw new ArgumentNullException(nameof(commodityService));
            _specialOffersService = specialOffersService ?? throw new ArgumentNullException(nameof(specialOffersService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _basketService = basketService ?? throw new ArgumentNullException(nameof(basketService));
            _busyService = busyService ?? throw new ArgumentNullException(nameof(busyService));
            InitializeCommand = new ActionCommand(async () => await Init());
            AddToBasketCommand = new DelegateCommand<CommodityListItemViewModel>(async item => await AddToBasketExecute(item));
            RemoveFromBasketCommand = new DelegateCommand<BasketListItemViewModel>(async item => await RemoveFromBasketExecute(item));
        }

        public bool IsBusy { get; set; }
        public IReadOnlyCollection<CommodityListItemViewModel> Commodities { get; set; }
        public IReadOnlyCollection<SpecialOfferListItemViewModel> SpecialOffers { get; set; }
        public BasketViewModel Basket { get; set; }
        public ICommand InitializeCommand { get; }
        public ICommand AddToBasketCommand { get; }
        public ICommand RemoveFromBasketCommand { get; }

        private async Task Init()
        {
            await _busyService.RunAsyncInBusyScopeWithCancellation(
                ct => Task.WhenAll(LoadCommodities(ct), LoadSpecialOffers(ct), LoadBasket(ct)),
                _busyService.DefaultScopeName, 
                "Loading...")
                .ConfigureAwait(false);

            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(Commodities)));
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(SpecialOffers)));
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(Basket)));
        }

        private async Task LoadCommodities(CancellationToken cancellationToken)
        {
            var commodities = await  _commodityService.GetBasicInfosAsync(cancellationToken).ConfigureAwait(false);
            Commodities = _mapper.Mapper.Map<IEnumerable<CommodityBasicInfo>, IEnumerable<CommodityListItemViewModel>>(commodities).ToList();
        }

        private async Task LoadSpecialOffers(CancellationToken cancellationToken)
        {
            var commodities = await _specialOffersService.GetBasicInfosAsync(cancellationToken).ConfigureAwait(false);
            SpecialOffers = _mapper.Mapper.Map<IEnumerable<SpecialOfferBasicInfo>, IEnumerable<SpecialOfferListItemViewModel>>(commodities).ToList();
        }

        private async Task LoadBasket(CancellationToken cancellationToken)
        {
            var basket = await _basketService.GetBasketContent(1, cancellationToken).ConfigureAwait(false);
            Basket = _mapper.Mapper.Map<BasketContent, BasketViewModel>(basket);
        }

        private async Task AddToBasketExecute(CommodityListItemViewModel item)
        {
            var basket = await _busyService.RunAsyncInBusyScope(
                _basketService.AddToBasket(1, item.Id, 1, default), 
                "BasketBusyScope", 
                "Adding to basket...");

            if (basket != null)
            {
                Basket = _mapper.Mapper.Map<BasketContent, BasketViewModel>(basket);
                OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(Basket)));
            }
        } 

        private async Task RemoveFromBasketExecute(BasketListItemViewModel item)
        {
            var basket = await _busyService.RunAsyncInBusyScope(
                 _basketService.RemoveFromBasket(1, item.CommodityId, 1, default),
                 "BasketBusyScope",
                 "Removing from basket...");

            if (basket != null)
            {
                Basket = _mapper.Mapper.Map<BasketContent, BasketViewModel>(basket);
                OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(nameof(Basket)));
            }
        }
    }
}