﻿namespace Ecommerce.Desktop.Client.ViewModels.Basket
{
    public class BasketListItemViewModel
    {
        public int CommodityId { get; private set; }
        public string CommodityName { get; private set; }
        public decimal CommodityTotalPrice { get; private set; }
        public decimal CommodityQuantity { get; private set; }
        public string QuantityUnit { get; private set; }
    }
}
