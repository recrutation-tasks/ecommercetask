﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.ViewModels.Basket
{
    public class BasketViewModel : Prism.Mvvm.BindableBase
    {
        public decimal TotalPrice { get; private set; }
        public IEnumerable<BasketListItemViewModel> OrderedCommodities { get; set; }
    }
}
