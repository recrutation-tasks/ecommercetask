﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.ViewModels.SpecialOffers
{
    public class SpecialOfferListItemViewModel
    {
        public int Id { get; private set; }
        public string Description { get; private set; }
    }
}
