﻿using AutoMapper;
using Ecommerce.Desktop.Client.AppServices;
using Ecommerce.Desktop.Client.Controls;
using Ecommerce.Desktop.Client.Mapping;
using Ecommerce.Desktop.Client.ViewModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Text;
using System.Windows;

namespace Ecommerce.Desktop.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public IServiceProvider ServiceProvider { get; private set; }
        public IConfiguration Configuration { get; private set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        protected override void OnStartup(StartupEventArgs args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory());

            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            ServiceProvider = serviceCollection.BuildServiceProvider();
            var MainWindow = ServiceProvider.GetRequiredService<MainWindow>();
            MainWindow.Show();
        }

        private void ConfigureServices(ServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<MainWindow>();
            serviceCollection.AddTransient<MainWindowViewModel>();
            serviceCollection.AddTransient<IServicesSettings, ServicesSettings>();
            serviceCollection.AddSingleton<ICommodityService, CommodityService>();
            serviceCollection.AddSingleton<ISpecialOffersService, SpecialOffersService>();
            serviceCollection.AddSingleton<IEcommerceAppMapper, EcommerceAppMapper>();
            serviceCollection.AddSingleton<IBasketService, BasketService>();
            UiControlsServicesInstaller.Install(serviceCollection);
        }
    }
}
