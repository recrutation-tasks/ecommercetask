﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Ecommerce.Desktop.Client.Converters
{
    public class MaxTwoDigitsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is null)
                return null;

            bool parsed = Double.TryParse(value.ToString(), out double parsedValue);
            if (!parsed)
                return null;

            return String.Format("{0:0.00}", parsedValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
