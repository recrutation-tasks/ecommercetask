using Ecommerce.Data.Model;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Describes quantity of goods
/// </summary>
public class QuantityDM : Entity
{

    /// <summary>
    /// Amount of goods
    /// </summary>
    public decimal Value { get; set; }
    /// <summary>
    /// Unit of measure
    /// </summary>
    public string UnitName { get; set; }
    /// <summary>
    /// Related commodity information
    /// </summary>
    public virtual CommodityDM Commodity { get; set; }
    /// <summary>
    /// Related commodityId
    /// </summary>
    [ForeignKey("Commodity")]
    public int? CommodityId { get; set; }
    /// <summary>
    /// Related commodity order information
    /// </summary>
    public virtual CommodityOrderDM CommodityOrder { get; set; }
    /// <summary>
    /// Related commodity order Id
    /// </summary>
    [ForeignKey("CommodityOrder")]
    public int? CommodityOrderId { get; set; }
}