using Ecommerce.Data.Model;

/// <summary>
/// Base information for every special offer
/// </summary>
public abstract class SpecialOfferDM : Entity
{
    /// <summary>
    /// Special offer description
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// Discount percentage value described as 0-1
    /// </summary>
    public decimal DiscountValue { get; set; }
    /// <summary>
    /// Id of commodity to be discounted
    /// </summary>
    public int? DiscountedCommodityId { get; set; }
    /// <summary>
    /// Amount of commodity discount applies to
    /// </summary>
    public decimal DiscountedAmount { get; set; }
    /// <summary>
    /// Type of special offer
    /// </summary>
    public SpecialOfferType SpecialOfferType { get; protected set; }
}