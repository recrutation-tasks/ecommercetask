﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Information about ordered commodity
    /// </summary>
    public class CommodityOrderDM : Entity
    {
        /// <summary>
        /// Related commodity information
        /// </summary>
        public CommodityDM Commodity { get; set; }
        /// <summary>
        /// Id of related commodity information
        /// </summary>
        [ForeignKey("Commodity")]
        public int CommodityId { get; set; }
        /// <summary>
        /// Quantity ordered
        /// </summary>
        public QuantityDM Quantity { get; set; }
        /// <summary>
        /// Related basket information
        /// </summary>
        public virtual BasketDM Basket { get; set; }
        /// <summary>
        /// Related basket Id
        /// </summary>
        [ForeignKey("Basket")]
        public int? BasketId { get; set; }
    }
}
