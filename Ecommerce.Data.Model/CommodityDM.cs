using Ecommerce.Data.Model;
using JetBrains.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Detailed information about commodity offered by store
/// </summary>
public class CommodityDM : Entity
{

    /// <summary>
    /// Commodity name
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Commodity description
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// Price per one unit of commodity in USD
    /// </summary>
    public decimal PricePerUnit { get; set; }
    ///// <summary>
    ///// Price Per Unit foreign key
    ///// </summary>
    public QuantityDM Quantity { get; set; }
} 