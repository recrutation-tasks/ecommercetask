﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Commodity order that is required for discount to be applied
    /// </summary>
    public class CommodityOrderRequirementDM : Entity
    {
        /// <summary>
        /// Related commodity information
        /// </summary>
        public CommodityDM Commodity { get; set; }
        /// <summary>
        /// Related commodity id
        /// </summary>
        [ForeignKey("CommodityId")]
        public int CommodityId { get; set; }
        /// <summary>
        /// Amount that has to be ordered for discount to be applied
        /// </summary>
        public decimal AmountRequired { get; set; }
        /// <summary>
        /// Required or any of marked
        /// </summary>
        public CalculationLogic CalculationLogic { get; set; }
        /// <summary>
        /// Related special offer
        /// </summary>
        public virtual BuySetOfProductsToGetDiscountOnThemSpecialOfferDM BuySetOfProductsToGetDiscountOnThemSpecialOffer { get; set; }
        /// <summary>
        /// Related special offer id
        /// </summary>
        [ForeignKey("BuySetOfProductsToGetDiscountOnThemSpecialOffer")]
        public int? BuySetOfProductsToGetDiscountOnThemSpecialOfferId { get; set; }
        /// <summary>
        /// Related special offer
        /// </summary>
        public virtual BuyOneProductAndGetDiscountOnOtherSpecialOfferDM BuyOneProductAndGetDiscountOnOtherSpecialOffer { get; set; }
        /// <summary>
        /// Related special offer id
        /// </summary>
        [ForeignKey("BuyOneProductAndGetDiscountOnOtherSpecialOffer")]
        public int? BuyOneProductAndGetDiscountOnOtherSpecialOfferId { get; set; }
    }
}
