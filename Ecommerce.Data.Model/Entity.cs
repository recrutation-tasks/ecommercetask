﻿using System.ComponentModel.DataAnnotations;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Base entity class
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        [Key]
        public int Id { get; set; }
    }
}
