﻿using System;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Active during certain period special offer
    /// </summary>
    public class ActiveDuringPeriodSpecialOfferDM : SpecialOfferDM
    {
        /// <summary>
        /// Creates new instance of ActiveDuringPeriodSpecialOfferDM
        /// </summary>
        public ActiveDuringPeriodSpecialOfferDM()
            => SpecialOfferType = SpecialOfferType.ActiveDuringPeriod;
        /// <summary>
        /// Start date of special offer
        /// </summary>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// End date of special offer
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}
