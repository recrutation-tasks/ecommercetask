﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Active after ordering specific goods special offer
    /// </summary>
    public class BuyOneProductAndGetDiscountOnOtherSpecialOfferDM : SpecialOfferDM
    {
        /// <summary>
        /// Creates new instance of BuyOneProductAndGetDiscountOnOtherSpecialOfferDM
        /// </summary>
        public BuyOneProductAndGetDiscountOnOtherSpecialOfferDM()
            => SpecialOfferType = SpecialOfferType.BuyOneProductAndGetDiscountOnOther;
        /// <summary>
        /// Id of commodities that have to be ordered
        /// </summary>
        [ForeignKey("RequiredCommoditiesId")]
        public int RequiredCommoditiesId { get; set; }
        /// <summary>
        /// Information about commodities that have to be ordered
        /// </summary>
        public CommodityOrderRequirementDM RequiredCommodities { get; private set; }
    }
}
