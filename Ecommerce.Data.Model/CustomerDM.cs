﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Customer information
    /// </summary>
    public class CustomerDM : Entity
    {
        /// <summary>
        /// Name of the customer
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Customer's basket
        /// </summary>
        public BasketDM Basket { get; set; }
        /// <summary>
        /// Customer's basked Id
        /// </summary>
        [ForeignKey("BasketId")]
        public int BasketId { get; set; }
    }
}
