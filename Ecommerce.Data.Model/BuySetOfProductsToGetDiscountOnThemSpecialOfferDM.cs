﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Active after ordering specific set of goods special offer
    /// </summary>
    public class BuySetOfProductsToGetDiscountOnThemSpecialOfferDM : SpecialOfferDM
    {
        /// <summary>
        /// Creates new instance of BuySetOfProductsToGetDiscountOnThemSpecialOfferDM
        /// </summary>
        public BuySetOfProductsToGetDiscountOnThemSpecialOfferDM() 
            => SpecialOfferType = SpecialOfferType.BuySetOfProductsToGetDiscountOnThem;

        /// <summary>
        /// Set of goods that have to be ordred
        /// </summary>
        public IEnumerable<CommodityOrderRequirementDM> RequiredCommodities { get; private set; }
    }
}
