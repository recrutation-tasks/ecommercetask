﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ecommerce.Data.Model
{
    /// <summary>
    /// Detailed information about customer basket. 
    /// </summary>
    public class BasketDM : Entity
    {
        /// <summary>
        /// Ordered commodities
        /// </summary>
        public IReadOnlyCollection<CommodityOrderDM> Commodities { get; set; }
        /// <summary>
        /// Total price of ordered commodities in USD ( includes discounts )
        /// </summary>
        public decimal TotalPrice { get; set; }
        /// <summary>
        /// Related customer information
        /// </summary>
        public CustomerDM Customer { get; set; }
        /// <summary>
        /// Customer Id
        /// </summary>
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
    }
}
