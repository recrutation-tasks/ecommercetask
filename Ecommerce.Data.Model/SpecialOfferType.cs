﻿namespace Ecommerce.Data.Model
{
    public enum SpecialOfferType
    {
        /// <summary>
        /// Special offer is active during certain perio od time
        /// </summary>
        ActiveDuringPeriod = 0,
        /// <summary>
        /// Special offer is active after ordering certain goods
        /// </summary>
        BuyOneProductAndGetDiscountOnOther = 1,
        /// <summary>
        /// Special offer is active after ordering certain set of goods
        /// </summary>
        BuySetOfProductsToGetDiscountOnThem = 2
    }
}
