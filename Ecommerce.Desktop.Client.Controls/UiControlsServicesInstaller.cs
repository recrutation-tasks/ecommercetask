﻿using Ecommerce.Desktop.Client.Controls.Helpers;
using Ecommerce.Desktop.Client.Controls.Services.BusyService;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Ecommerce.Desktop.Client.Controls
{
    public static class UiControlsServicesInstaller
    {
        public static void Install([NotNull] ServiceCollection serviceCollection)
        {
            if (serviceCollection is null)
                throw new ArgumentNullException(nameof(serviceCollection));

            serviceCollection.AddTransient<IUiHelper, UIhelper>();
            serviceCollection.AddTransient<IDispatcherProvider, DispatcherProvider>();
            serviceCollection.AddTransient<IBusyContextFactory, BusyContextFactory>();
            serviceCollection.AddSingleton<IBusyMaskManager, BusyMaskManager>();
            serviceCollection.AddSingleton<IBusyService, BusyService>();
        }
    }
}
