﻿using BusyIndicator;
using Ecommerce.Desktop.Client.Controls.Helpers;
using JetBrains.Annotations;
using Microsoft.Xaml.Behaviors.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public class BusyMaskManager : IBusyMaskManager
    {
        private readonly IUiHelper _uiHelper;
        private readonly Dictionary<string, (List<BusyContext> Contexts, ICommand? CancellationCommand)> _activeScopes = new();

        public BusyMaskManager(IUiHelper uiHelper)
        {
            _uiHelper = uiHelper ?? throw new ArgumentNullException(nameof(uiHelper));
        }

        public void SubscribeToMask([NotNull] BusyContext context)
        {
            var scopeExists = _activeScopes.TryGetValue(
                context.ScopeName,
                out var scope);

            BusyContext topContext = null;

            if (scopeExists)
            {
                scope.Contexts.Add(context);
            }
            else
            {
                List<BusyContext> contexts = new List<BusyContext>();
                contexts.Add(context);
                scope = (contexts, new ActionCommand(() => contexts.LastOrDefault()?.RequestCancel()));
                _activeScopes.Add(context.ScopeName, scope);
            }

            var mask = _uiHelper.TryFindInAllWindows<BusyMask>(context.ScopeName);

            if (mask != null)
            {
                mask.IsBusy = true;
                mask.BusyContent = context.Reason;
                var cancellationMask = mask as CancellationBusyMask;
                if (cancellationMask != null)
                {
                    cancellationMask.AllowsCancellation = context.AllowsCancellation;
                    cancellationMask.CancelCommand = scope.CancellationCommand;
                }
            }
        }

        public void UnsubscribeToMask([NotNull] BusyContext context)
        {
            var scopeExists = _activeScopes.TryGetValue(context.ScopeName, out var scope);
            if (!scopeExists || !scope.Contexts.Contains(context))
            {
                if (context != null)
                    context.Dispose();
                return;
            }

            scope.Contexts.Remove(context);
            var lastAction = scope.Contexts.LastOrDefault();

            var mask = _uiHelper.TryFindInAllWindows<BusyMask>(context.ScopeName);
            if (mask != null)
            {
                mask.IsBusy = lastAction != null;
                mask.BusyContent = lastAction?.Reason;
            }

            if (!scope.Contexts.Any())
            {
                _activeScopes.Remove(context.ScopeName);
                scope.CancellationCommand = null;
            }

            if (context != null)
                context.Dispose();

            return;
        }
    }
}
