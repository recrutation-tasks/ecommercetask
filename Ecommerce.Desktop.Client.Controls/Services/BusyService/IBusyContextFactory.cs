﻿using JetBrains.Annotations;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public interface IBusyContextFactory
    {
        BusyContext CreateWithNoCancellation(
            [NotNull] Task associatedJob,
            [CanBeNull] string scopeName,
            [CanBeNull] string busyReason);

        BusyContext CreateWithCancellation(
            [NotNull] Task associatedJob,
            [CanBeNull] string scopeName,
            [CanBeNull] string busyReason,
            [NotNull] CancellationTokenSource tokenSource);
    }
}
