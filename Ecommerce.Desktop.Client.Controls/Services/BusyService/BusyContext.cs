﻿using JetBrains.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public class BusyContext : IDisposable
    {
        private bool _disposedValue;

        private CancellationTokenSource? _tokenSource;
        private WeakReference<Task> _associatedJobReference;
        private readonly string _scopeName;
        private readonly string _busyReason;

        public CancellationToken Token => _tokenSource != null ? _tokenSource.Token : default;
        public bool AllowsCancellation => _tokenSource != null;
        public string ScopeName => _scopeName;
        public string Reason => _busyReason;
        public bool IsBusy
        {
            get
            {
                _associatedJobReference.TryGetTarget(out Task? job);
                return job != null && Constants.BusyStatuses.Contains(job.Status);
            }
        }
        public event Action<BusyContext>? JobCompleted;

        public BusyContext(
            [NotNull] Task associatedJob,
            [NotNull] string scopeName,
            [CanBeNull] string busyReason,
            [CanBeNull] CancellationTokenSource? tokenSource)
        {
            if (string.IsNullOrWhiteSpace(scopeName))
                throw new ArgumentNullException(nameof(scopeName));

            if (associatedJob is null)
                throw new ArgumentNullException(nameof(associatedJob));

            _tokenSource = tokenSource;
            AddJob(associatedJob);
            _busyReason = busyReason;
            _scopeName = scopeName;
        }

        private void AddJob([NotNull] Task associatedJob)
        {
            if (_associatedJobReference is null)
                _associatedJobReference = new WeakReference<Task>(associatedJob);
            else
                _associatedJobReference.SetTarget(associatedJob);

            if (IsBusy)
            {
                _associatedJobReference.TryGetTarget(out Task? job);
                if (job != null)
                    job.GetAwaiter().OnCompleted(() => JobCompleted?.Invoke(this));
            }
        }

        public void RequestCancel()
        {
            if (_tokenSource != null && !_tokenSource.IsCancellationRequested)
                _tokenSource.Cancel();
        }

        public void Dispose() => Dispose(true);

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _tokenSource?.Dispose();
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
                    _associatedJobReference?.SetTarget(null);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.
                }

                _disposedValue = true;
            }
        }
    }
}
