﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    internal static class Constants
    {
        public static readonly string DefaultScopeName = "MainBusyScope";

        public static readonly ReadOnlyCollection<TaskStatus> BusyStatuses = new ReadOnlyCollection<TaskStatus>(new List<TaskStatus> {
            TaskStatus.Running,
            TaskStatus.Created,
            TaskStatus.WaitingForActivation,
            TaskStatus.WaitingForChildrenToComplete,
            TaskStatus.WaitingToRun
        });
    }
}
