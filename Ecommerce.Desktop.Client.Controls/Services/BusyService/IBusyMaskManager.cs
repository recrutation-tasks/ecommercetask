﻿using JetBrains.Annotations;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public interface IBusyMaskManager
    {
        void SubscribeToMask([NotNull] BusyContext context);

        void UnsubscribeToMask([NotNull] BusyContext context);
    }
}
