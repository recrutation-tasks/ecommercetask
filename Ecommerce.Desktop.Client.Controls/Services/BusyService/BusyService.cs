﻿using Ecommerce.Desktop.Client.Controls.Helpers;
using JetBrains.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public sealed class BusyService : IBusyService
    {
        private readonly IBusyContextFactory _contextFactory;
        private readonly IDispatcherProvider _dispatcherProvider;
        private readonly IBusyMaskManager _busyMaskManager;

        public string DefaultScopeName { get; set; } = Constants.DefaultScopeName;

        public BusyService(
            [NotNull] IBusyContextFactory busyContextFactory, 
            [NotNull] IDispatcherProvider dispatcherProvider,
            [NotNull] IBusyMaskManager busyMaskManager)
        {
            _contextFactory = busyContextFactory ?? throw new ArgumentNullException(nameof(busyContextFactory));
            _dispatcherProvider = dispatcherProvider ?? throw new ArgumentNullException(nameof(dispatcherProvider));
            _busyMaskManager = busyMaskManager ?? throw new ArgumentNullException(nameof(busyMaskManager));
        }

        public T RunAsyncInBusyScope<T>(
            [NotNull] T associatedJob,
            [CanBeNull] string busyScopeName = "",
            [CanBeNull] string reason = "")
            where T : Task
        {
            if (associatedJob is null)
                throw new ArgumentNullException(nameof(associatedJob));

            InitializeContext(
                _contextFactory.CreateWithNoCancellation(
                    associatedJob, 
                    busyScopeName, 
                    reason));

            return associatedJob;
        }

        public T RunAsyncInBusyScopeWithCancellation<T>(
            [NotNull] Func<CancellationToken, T> associatedJob,
            [CanBeNull] string busyScopeName = "",
            [CanBeNull] string reason = "")
            where T : Task
        {
            if (associatedJob is null)
                throw new ArgumentNullException(nameof(associatedJob));

            var tokenSource = new CancellationTokenSource();
            var task = associatedJob(tokenSource.Token);

            InitializeContext(
                _contextFactory.CreateWithCancellation(
                    task, 
                    busyScopeName, 
                    reason, 
                    tokenSource));

            return task;
        }

        private void InitializeContext([NotNull] BusyContext busyContext)
        {
            if (!busyContext.IsBusy)
                return;

            busyContext.JobCompleted += BusyAction_JobCompleted;

            _dispatcherProvider.PostToApplicationDispatcher(
                () => _busyMaskManager.SubscribeToMask(busyContext));
        }

        private void BusyAction_JobCompleted(BusyContext context)
        {
            context.JobCompleted -= BusyAction_JobCompleted;
            _dispatcherProvider
                .PostToApplicationDispatcher(
                () => _busyMaskManager.UnsubscribeToMask(context));
        }
    }
}
