﻿using JetBrains.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public interface IBusyService
    {
        string DefaultScopeName { get; set; }

        T RunAsyncInBusyScopeWithCancellation<T>(
            [NotNull] Func<CancellationToken, T> associatedJob,
            [CanBeNull] string busyScopeName = "",
            [CanBeNull] string reason = "")
            where T : Task;

        T RunAsyncInBusyScope<T>(
            [NotNull] T associatedJob,
            [CanBeNull] string busyScopeName = "",
            [CanBeNull] string reason = "")
            where T : Task;
    }
}
