﻿using JetBrains.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ecommerce.Desktop.Client.Controls.Services.BusyService
{
    public class BusyContextFactory : IBusyContextFactory
    {
        public BusyContext CreateWithNoCancellation(
            [NotNull] Task associatedJob,
            [CanBeNull] string scopeName,
            [CanBeNull] string busyReason)
        {
            if (associatedJob is null)
                throw new ArgumentNullException(nameof(associatedJob));

            return new BusyContext(associatedJob, scopeName, busyReason, null);
        }

        public BusyContext CreateWithCancellation(
            [NotNull] Task associatedJob,
            [CanBeNull] string scopeName,
            [CanBeNull] string busyReason,
            [NotNull] CancellationTokenSource tokenSource)
        {
            if (associatedJob is null)
                throw new ArgumentNullException(nameof(associatedJob));

            if (tokenSource is null)
                throw new ArgumentNullException(nameof(tokenSource));

            return new BusyContext(associatedJob, scopeName, busyReason, tokenSource);
        }
    }
}
