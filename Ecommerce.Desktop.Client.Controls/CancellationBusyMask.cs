﻿using BusyIndicator;
using System.Windows.Input;
using System.Windows;

namespace Ecommerce.Desktop.Client.Controls
{
    public class CancellationBusyMask : BusyMask
    {
        public static readonly DependencyProperty CancelCommandProperty
            = DependencyProperty.Register(
            "CancelCommand",
            typeof(ICommand),
            typeof(CancellationBusyMask));

        public static readonly DependencyProperty AllowsCancellationProperty
            = DependencyProperty.Register(
            "AllowsCancellation",
            typeof(bool),
            typeof(CancellationBusyMask));

        public ICommand CancelCommand
        {
            get => (ICommand)GetValue(CancelCommandProperty);
            set => SetValue(CancelCommandProperty, value);
        }

        public bool AllowsCancellation
        {
            get => (bool)GetValue(AllowsCancellationProperty);
            set => SetValue(AllowsCancellationProperty, value);
        }
    }
}
