﻿using System;

namespace Ecommerce.Desktop.Client.Controls.Helpers
{
    public interface IDispatcherProvider
    {
        void PostToApplicationDispatcher(Delegate method);
    }
}
