﻿using System;
using System.Windows;

namespace Ecommerce.Desktop.Client.Controls.Helpers
{
    public class DispatcherProvider : IDispatcherProvider
    {
        public void PostToApplicationDispatcher(Delegate method)
            => Application.Current.Dispatcher.BeginInvoke(method);
    }
}
