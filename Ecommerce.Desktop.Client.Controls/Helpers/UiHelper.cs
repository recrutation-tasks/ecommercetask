﻿using System;
using System.Windows.Media;
using System.Windows;
using BusyIndicator;
using JetBrains.Annotations;

namespace Ecommerce.Desktop.Client.Controls.Helpers
{
    public class UIhelper : IUiHelper
    {
        public T? FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject
        {
            if (parent == null) return null;

            T? foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                T? childType = child as T;
                if (childType == null)
                {
                    foundChild = FindChild<T>(child, childName);

                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        foundChild = (T)child;
                        break;
                    }

                    foundChild = FindChild<T>(child, childName);
                    if (foundChild != null) break;


                    else
                    {
                        foundChild = (T)child;
                        break;
                    }
                }
            }

            return foundChild;
        }

        public T? TryFindInAllWindows<T>([CanBeNull] string name) where T : DependencyObject
        {
            var windows = Application.Current.Windows;

            foreach (var window in windows)
            {
                var child = FindChild<T>((DependencyObject)window, name);
                if (child != null)
                    return child;
            }

            return null;
        }
    }
}
