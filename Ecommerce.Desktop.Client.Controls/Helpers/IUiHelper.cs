﻿using JetBrains.Annotations;
using System;
using System.Windows;

namespace Ecommerce.Desktop.Client.Controls.Helpers
{
    public interface IUiHelper
    {
        T? FindChild<T>(DependencyObject parent, string childName) where T : DependencyObject;

        T? TryFindInAllWindows<T>([CanBeNull] string name) where T : DependencyObject;
    }
}
