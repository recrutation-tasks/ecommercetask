using Ecommerce.DataTransfer.Model;
using Ecommerce.Domain.Model.SpecialOfferContext;
using Ecommerce.SpecialOffer.API.Domain.Services;
using FluentAssertions;

namespace Ecommerce.SpecialOffer.API.UnitTests
{
    [TestFixture]
    public class AdpDiscountSelectorTests
    {
        private static DateTime s_validStartDate = DateTime.UtcNow.AddDays(-3);
        private static DateTime s_validEndDate = DateTime.UtcNow.AddDays(3);

        private static Ecommerce.Domain.Model.SpecialOfferContext.ActiveDuringPeriodSpecialOffer GetDefaultAdpOffer(DateTime? startDate, DateTime? endDate)
            => new Ecommerce.Domain.Model.SpecialOfferContext.ActiveDuringPeriodSpecialOffer(
                1,
                "Apples have 10% discount this week",
                0.1m,
                1,
                9999,
                startDate,
                endDate);

        private static IReadOnlyCollection<CommodityOrderBasicInfo> GetDefaultOrders()
            => new List<CommodityOrderBasicInfo>()
               {
                    new CommodityOrderBasicInfo
                    {
                        CommodityId = 1,
                        Quantity = 10
                    }
               };

        private static DiscountBasicInfo GetDefaultDiscount()
            => new DiscountBasicInfo
            {
                CommodityId = 1,
                Amount = 9999,
                Percentage = 0.1m
            };

        public static IEnumerable<(DateTime? startDate, DateTime? endDate)> GetValidDatesTestCases() =>
            new List<(DateTime? startDate, DateTime? endDate)>()
            {
                (null, DateTime.UtcNow.AddDays(7)),
                (DateTime.UtcNow.AddDays(-3), null),
                (DateTime.UtcNow.AddDays(-3), DateTime.UtcNow.AddDays(3))
            };

    [Test]
        public void Get_Should_ThrowArgException_When_OfferIsNull()
        {
            //arrange
            var strategy = new AdpDiscountSelectorStrategy();
            var orders = new List<CommodityOrderBasicInfo>();
            ActiveDuringPeriodSpecialOffer offer = null;
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            Action act = () => strategy.Get(offer, orders);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

            //act, assert
            act.Should().Throw<ArgumentNullException>();
        }

        [Test]
        public void Get_Should_ReturnEmptyCollection_When_CurrentOrdersArgisNull()
        {
            //arrange
            var strategy = new AdpDiscountSelectorStrategy();
            IReadOnlyCollection<CommodityOrderBasicInfo> orders = null;
            var offer = GetDefaultAdpOffer(s_validStartDate, DateTime.UtcNow.AddDays(-1));

            //act
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            var result = strategy.Get(offer, orders);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

            //assert
            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        [Test]
        public void Get_Should_ReturnEmptyCollection_When_UtcStartDateIsGreaterThanCurrentTime()
        {
            //arrange
            var strategy = new AdpDiscountSelectorStrategy();
            var orders = GetDefaultOrders();
            var offer = GetDefaultAdpOffer(s_validStartDate, DateTime.UtcNow.AddDays(-1));

            //act
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            var result = strategy.Get(offer, orders);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

            //assert
            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        [Test]
        public void Get_Should_ReturnEmptyCollection_When_UtcEndDateIsLowerThanCurrentTime()
        {
            //arrange
            var strategy = new AdpDiscountSelectorStrategy();
            var orders = GetDefaultOrders();
            var offer = GetDefaultAdpOffer(s_validStartDate, DateTime.UtcNow.AddDays(-1));

            //act
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            var result = strategy.Get(offer, orders);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

            //assert
            result.Should().NotBeNull();
            result.Should().BeEmpty();
        }

        [Test]
        [TestCaseSource(nameof(GetValidDatesTestCases))]
        public void Get_Should_ReturnSingleDiscount_When_DatesAreValid((DateTime? startDate, DateTime? endDate) testCase)
        {
            //arrange
            var strategy = new AdpDiscountSelectorStrategy();
            var orders = GetDefaultOrders();
            var offer = GetDefaultAdpOffer(testCase.startDate, testCase.endDate);

            //act
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            var result = strategy.Get(offer, orders);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

            //assert
            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(GetDefaultDiscount());
         }

        [Test]
        [TestCaseSource(nameof(GetValidDatesTestCases))]
        public void Get_Should_ReturnSingleDiscount_When_DatesAreValidAndBasketContainsOtherProducts((DateTime? startDate, DateTime? endDate) testCase)
        {
            //arrange
            var strategy = new AdpDiscountSelectorStrategy();
            var orders = GetDefaultOrders().ToList();
            orders.Add(new CommodityOrderBasicInfo
            {
                CommodityId = 2,
                Quantity = 5
            });
            orders.Add(new CommodityOrderBasicInfo
            {
                CommodityId = 3,
                Quantity = 5
            });
            var offer = GetDefaultAdpOffer(testCase.startDate, testCase.endDate);

            //act
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            var result = strategy.Get(offer, orders);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.

            //assert
            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(GetDefaultDiscount());
        }
    }
}