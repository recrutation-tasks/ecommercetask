﻿using Ecommerce.DataTransfer.Model;
using MediatR;

namespace Ecommerce.Basket.API.Domain.Queries
{
    public class GetBasketContentQuery : IRequest<BasketContent>
    {
        public GetBasketContentQuery(int customerId)
        {
            CustomerId = customerId;
        }

        public int CustomerId { get; }
    }
}
