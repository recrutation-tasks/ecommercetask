﻿using AutoMapper;
using Ecommerce.Basket.API.Domain.Contexts;
using Ecommerce.Basket.API.Domain.Services;
using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using MediatR;

namespace Ecommerce.Basket.API.Domain.Queries
{
    public class GetBasketContentQueryHandler : IRequestHandler<GetBasketContentQuery, BasketContent>
    {
        private readonly IDomainContext _context;
        private readonly IPriceCalculationService _priceCalculation;
        private readonly IMapper _mapper;

        public GetBasketContentQueryHandler([NotNull] IDomainContext context,[NotNull] IPriceCalculationService priceCalculation, [NotNull] IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _priceCalculation = priceCalculation ?? throw new ArgumentNullException(nameof(priceCalculation));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<BasketContent> Handle(GetBasketContentQuery request, CancellationToken cancellationToken)
        {
            var basket = _context.GetBasketContent(request.CustomerId);
            await _priceCalculation.CalculatePrices(basket, cancellationToken);

            return _mapper.Map<Ecommerce.Domain.Model.BasketContext.Basket, BasketContent>(basket);
        }
    }
}
