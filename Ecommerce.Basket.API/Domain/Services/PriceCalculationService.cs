﻿using Ecommerce.Database;
using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;

namespace Ecommerce.Basket.API.Domain.Services
{
    public class PriceCalculationService : IPriceCalculationService
    {
        private readonly IEcommerceStorageContext _storageContext;
        private readonly IDiscountService _discountService;

        public PriceCalculationService([NotNull] IEcommerceStorageContext storageContext, [NotNull] IDiscountService discountService)
        {
            _storageContext = storageContext ?? throw new ArgumentNullException(nameof(storageContext));
            _discountService = discountService ?? throw new ArgumentNullException(nameof(discountService));
        }

        public async Task CalculatePrices(Ecommerce.Domain.Model.BasketContext.Basket basket, CancellationToken cancellationToken)
        {
            decimal total = 0;
            var orderedCommodities = _storageContext.Get<CommodityDM>().Where(x => basket.OrderedCommodities.Select(c => c.CommodityId).Contains(x.Id)).ToList();
            var discountsApplied = await _discountService.GetDiscounts(basket, cancellationToken).ConfigureAwait(true);

            foreach(var commodityOrder in basket.OrderedCommodities)
            {
                var discount = discountsApplied.FirstOrDefault(x => x.CommodityId == commodityOrder.CommodityId);

                if (discount != null && discount.Amount >= commodityOrder.CommodityQuantity)
                    discount.Amount = commodityOrder.CommodityQuantity;
                var discountedValue = discount != null ? 1 - (discount.Amount / commodityOrder.CommodityQuantity) * discount.Percentage : 1;
                decimal price = discountedValue * commodityOrder.CommodityPrice * commodityOrder.CommodityQuantity ;
                commodityOrder.SetPrice(price);
                total += price;
            }

            basket.SetPrice(total);
        }
    }
}
