﻿namespace Ecommerce.Basket.API.Domain.Services
{
    public interface IPriceCalculationService
    {
        Task CalculatePrices(Ecommerce.Domain.Model.BasketContext.Basket basket, CancellationToken cancellationToken);
    }
}
