﻿using Ecommerce.DataTransfer.Model;

namespace Ecommerce.Basket.API.Domain.Services
{
    public interface IDiscountService
    {
        Task<IReadOnlyCollection<DiscountBasicInfo>> GetDiscounts(Ecommerce.Domain.Model.BasketContext.Basket basket, CancellationToken cancellationToken);
    }
}
