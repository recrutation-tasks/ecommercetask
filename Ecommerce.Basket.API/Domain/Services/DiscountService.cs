﻿using AutoMapper;
using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace Ecommerce.Basket.API.Domain.Services
{
    public class DiscountService : IDiscountService
    {
        private const string JsonMediaType = "application/json";
        private const string DiscountsResourcePath = "/Discounts/Applied";
        private readonly string _discountEndpointUrl = "https://localhost:7112/api/v1";
        private readonly int _retries = 3;
        private readonly AsyncRetryPolicy<HttpResponseMessage> _retryPolicy;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IMapper _mapper;

        public DiscountService([NotNull] IMapper mapper, [NotNull] IHttpClientFactory httpClientFactory)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _httpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
            _retryPolicy = Policy<HttpResponseMessage>
                .Handle<HttpRequestException>()
                .OrResult(r => r == null || !r.IsSuccessStatusCode)
                .WaitAndRetryAsync(_retries, count => TimeSpan.FromMilliseconds(500));
        }

        public async Task<IReadOnlyCollection<DiscountBasicInfo>> GetDiscounts(Ecommerce.Domain.Model.BasketContext.Basket basket, CancellationToken cancellationToken)
        {
            var jsonOrders = JsonConvert.SerializeObject(_mapper.Map<IEnumerable<Ecommerce.Domain.Model.BasketContext.CommodityOrder>, IEnumerable<CommodityOrderBasicInfo>>(basket.OrderedCommodities));
            StringContent content = new StringContent(jsonOrders, Encoding.UTF8, JsonMediaType);
            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JsonMediaType));

            var response = await _retryPolicy.ExecuteAsync(async () =>
            {
                return await client.PostAsync(_discountEndpointUrl + DiscountsResourcePath, content);
            });

            if (response is null || response.StatusCode != HttpStatusCode.OK)
                throw new HttpRequestException($"Remote call to {_discountEndpointUrl + DiscountsResourcePath} failed with status code {response?.StatusCode}");

            return JsonConvert.DeserializeObject<IEnumerable<DiscountBasicInfo>>(await response.Content.ReadAsStringAsync()).ToList();
        }
    }
}
