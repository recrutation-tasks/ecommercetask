﻿using Ecommerce.Data.Model;
using Ecommerce.Database;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Basket.API.Domain.Contexts
{
    public class DomainContext : IDomainContext
    {
        private readonly IEcommerceStorageContext _storageContext;

        public DomainContext([NotNull] IEcommerceStorageContext storageContext)
        {
            _storageContext = storageContext ?? throw new ArgumentNullException(nameof(storageContext));
        }

        public void AddToBasket(int customerId, int commodityId, decimal quantity)
        {
            var quantityUnit = _storageContext.Get<QuantityDM>().FirstOrDefault(x => x.CommodityId == commodityId);
            if (quantityUnit is null)
                throw new ArgumentException(nameof(commodityId), "Cannot add non existing commodity to basket");
            var basketModel = _storageContext.Get<BasketDM>().Where(b => b.CustomerId == customerId).Include(x => x.Commodities).Include(x => x.Customer).First();
            var existingCommodity = basketModel.Commodities.FirstOrDefault(x => x.CommodityId == commodityId);
            

            if (existingCommodity == null)
            {
                _storageContext.Get<CommodityOrderDM>().Add(new CommodityOrderDM
                {
                    CommodityId = commodityId,
                    BasketId = basketModel.Id,
                    Quantity = new QuantityDM
                    {
                        Value = quantity,
                        UnitName = quantityUnit.UnitName
                    }
                });
            } 
            else
            {
                var existingQuantity = _storageContext.Get<QuantityDM>().First(q => q.CommodityOrderId == existingCommodity.Id);
                existingQuantity.Value = existingQuantity.Value + quantity;
            }
            
            _storageContext.SaveChanges();
        }

        public void RemoveFromBasket(int customerId, int commodityId, decimal quantity)
        {
            var basketModel = _storageContext.Get<BasketDM>().Where(b => b.CustomerId == customerId).Include(x => x.Commodities).Include(x => x.Customer).First();
            var existingCommodity = basketModel.Commodities.First(x => x.CommodityId == commodityId);
            var existingQuantity = _storageContext.Get<QuantityDM>().First(q => q.CommodityOrderId == existingCommodity.Id);
            if (existingQuantity.Value > quantity)
            {
                existingQuantity.Value = existingQuantity.Value - quantity;
            }
            else
            {
                _storageContext.Get<QuantityDM>().Remove(existingQuantity);
                _storageContext.Get<CommodityOrderDM>().Remove(existingCommodity);
            }

            _storageContext.SaveChanges();
        }

        public Ecommerce.Domain.Model.BasketContext.Basket GetBasketContent(int customerId)
        {
            var basketModel = _storageContext.Get<BasketDM>().Where(b => b.CustomerId == customerId).Include(x => x.Commodities).Include(x => x.Customer).First();
            var includedCommodities = _storageContext.Get<CommodityDM>().Where(x => basketModel.Commodities.Select(c => c.CommodityId).Contains(x.Id)).ToList();
            var includedQuantities = _storageContext.Get<QuantityDM>().Where(x => x.CommodityOrderId != null && basketModel.Commodities.Select(c => c.Id).Contains(x.CommodityOrderId.Value)).ToList();

            foreach(var cOrder in basketModel.Commodities)
            {
                cOrder.Commodity = includedCommodities.First(x => cOrder.CommodityId == x.Id);
                cOrder.Quantity = includedQuantities.First(x => x.CommodityOrderId == cOrder.Id);
            }
            return new Ecommerce.Domain.Model.BasketContext.Basket(basketModel.Id, basketModel.CustomerId,
                basketModel.Commodities.Select(c => new Ecommerce.Domain.Model.BasketContext.CommodityOrder(c.Id, c.CommodityId, c.Quantity.Value, c.Commodity.Name, c.Quantity.UnitName, c.Commodity.PricePerUnit)).ToList());
        }
    }
}
