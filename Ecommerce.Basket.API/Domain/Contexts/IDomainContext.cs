﻿namespace Ecommerce.Basket.API.Domain.Contexts
{
    public interface IDomainContext
    {
        Ecommerce.Domain.Model.BasketContext.Basket GetBasketContent(int customerId);
        void AddToBasket(int cutomerId, int commodityId, decimal quantity);
        void RemoveFromBasket(int customerId, int commodityId, decimal quantity);
    }
}
