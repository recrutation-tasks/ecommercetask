﻿using Ecommerce.Data.Model;
using Ecommerce.DataTransfer.Model;
using MediatR;

namespace Ecommerce.Basket.API.Domain.Commands
{
    public class RemoveFromBasketCommand : IRequest<BasketContent>
    {
        public RemoveFromBasketCommand(int customerId, int commodityId, decimal quantity)
        {
            CustomerId = customerId;
            CommodityId = commodityId;
            Quantity = quantity;
        }

        public int CustomerId { get; init; }
        public int CommodityId { get; init; }
        public decimal Quantity { get; init; }
    }
}
