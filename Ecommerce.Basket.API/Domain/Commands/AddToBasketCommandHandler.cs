﻿using AutoMapper;
using Ecommerce.Basket.API.Domain.Contexts;
using Ecommerce.Basket.API.Domain.Services;
using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using MediatR;

namespace Ecommerce.Basket.API.Domain.Commands
{
    public class AddToBasketCommandHandler : IRequestHandler<AddToBasketCommand, BasketContent>
    {
        private readonly IDomainContext _context;
        private readonly IPriceCalculationService _priceCalculation;
        private readonly IMapper _mapper;

        public AddToBasketCommandHandler([NotNull] IDomainContext context, [NotNull] IPriceCalculationService priceCalculationService, [NotNull] IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _priceCalculation = priceCalculationService ?? throw new ArgumentNullException(nameof(priceCalculationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<BasketContent> Handle(AddToBasketCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            if (cancellationToken.IsCancellationRequested)
                throw new OperationCanceledException();


            _context.AddToBasket(request.CustomerId, request.CommodityId, request.Quantity);
            var basket = _context.GetBasketContent(request.CustomerId);
            await _priceCalculation.CalculatePrices(basket, cancellationToken);

            return _mapper.Map<Ecommerce.Domain.Model.BasketContext.Basket, BasketContent>(basket);
        }
    }
}
