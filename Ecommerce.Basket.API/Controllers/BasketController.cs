﻿using Ecommerce.Basket.API.Domain.Commands;
using Ecommerce.Basket.API.Domain.Queries;
using JetBrains.Annotations;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Basket.API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BasketController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BasketController([NotNull] IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [Route("BasketContent/{customerId:int}")]
        [HttpGet]
        public async Task<IActionResult> BasketContent(int customerId, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetBasketContentQuery(customerId), cancellationToken));
        }

        [Route("AddToBasket/{customerId:int}/{commodityId:int}/{quantity:decimal}")]
        [HttpPatch]
        public async Task<IActionResult> AddToBasket(int customerId, int commodityId, decimal quantity, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new AddToBasketCommand(customerId, commodityId, quantity), cancellationToken));
        }

        [Route("RemoveFromBasket/{customerId:int}/{commodityId:int}/{quantity:decimal}")]
        [HttpPatch]
        public async Task<IActionResult> RemoveFromBasket(int customerId, int commodityId, decimal quantity, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new RemoveFromBasketCommand(customerId, commodityId, quantity), cancellationToken));
        }
    }
}
