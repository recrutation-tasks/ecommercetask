﻿using AutoMapper;
using Ecommerce.DataTransfer.Model;

namespace Ecommerce.Basket.API
{
    public class BasketProfile : Profile
    {
        public BasketProfile()
        {
            CreateMap<Ecommerce.Domain.Model.BasketContext.Basket, BasketContent>();
            CreateMap<Ecommerce.Domain.Model.BasketContext.CommodityOrder, Ecommerce.DataTransfer.Model.CommodityOrder>();
            CreateMap<Ecommerce.Domain.Model.BasketContext.CommodityOrder, CommodityOrderBasicInfo>()
               .ForMember(x => x.Quantity, opt => opt.MapFrom(c => c.CommodityQuantity));
        }
    }
}
