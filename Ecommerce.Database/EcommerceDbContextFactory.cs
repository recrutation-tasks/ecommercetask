﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Database
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<EcommerceContext>
    {
        public EcommerceContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<EcommerceContext>();
            var connectionString = "Data Source=localhost,1433;Initial Catalog=EcommerceDb;Integrated Security=False;User Id=sa;Password=Prop50i1041@;MultipleActiveResultSets=True";
            builder.UseSqlServer(connectionString);
            return new EcommerceContext(builder.Options);
        }
    }
}
