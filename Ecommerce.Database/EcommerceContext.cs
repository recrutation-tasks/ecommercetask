﻿using Ecommerce.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Database
{
    public class EcommerceContext : DbContext, IEcommerceStorageContext
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public DbSet<ActiveDuringPeriodSpecialOfferDM> ActiveDuringPeriodSpecialOffers { get; set; }
        public DbSet<CommodityDM> Commodities { get; set; }
        public DbSet<BuyOneProductAndGetDiscountOnOtherSpecialOfferDM > BuyOneProductAndGetDiscountOnOtherSpecialOffers { get; set; }
        public DbSet<BasketDM> Baskets { get; set; }
        public DbSet<QuantityDM> Quantities { get; set; }
        public DbSet<SpecialOfferDM> SpecialOffers { get; set; }
        public DbSet<BuySetOfProductsToGetDiscountOnThemSpecialOfferDM> BuySetOfProductsToGetDiscountOnThemSpecialOffers { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.


        public EcommerceContext(DbContextOptions<EcommerceContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActiveDuringPeriodSpecialOfferDM>().ToTable(nameof(ActiveDuringPeriodSpecialOffers));
            modelBuilder.Entity<CommodityDM>().ToTable(nameof(Commodities));
            modelBuilder.Entity<BuyOneProductAndGetDiscountOnOtherSpecialOfferDM>().ToTable(nameof(BuyOneProductAndGetDiscountOnOtherSpecialOffers));
            modelBuilder.Entity<BasketDM>().ToTable(nameof(Baskets));
            modelBuilder.Entity<QuantityDM>().ToTable(nameof(Quantities));
            modelBuilder.Entity<SpecialOfferDM>().ToTable(nameof(SpecialOffers));
            modelBuilder.Entity<BuySetOfProductsToGetDiscountOnThemSpecialOfferDM>().ToTable(nameof(BuySetOfProductsToGetDiscountOnThemSpecialOffers));

            var dataBuilder = new EcommerceDataBuilder(modelBuilder);
            dataBuilder.CreateInitialData();
        }

        public DbSet<T> Get<T>() where T : Entity
            => Set<T>();

        void IEcommerceStorageContext.SaveChanges() => SaveChanges();
    }
}
