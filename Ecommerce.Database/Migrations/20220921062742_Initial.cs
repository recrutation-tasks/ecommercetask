﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ecommerce.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Commodities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PricePerUnit = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commodities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerDM",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BasketId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerDM", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SpecialOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DiscountValue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DiscountedCommodityId = table.Column<int>(type: "int", nullable: true),
                    DiscountedAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    SpecialOfferType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecialOffers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Baskets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CustomerId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Baskets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Baskets_CustomerDM_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "CustomerDM",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActiveDuringPeriodSpecialOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActiveDuringPeriodSpecialOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActiveDuringPeriodSpecialOffers_SpecialOffers_Id",
                        column: x => x.Id,
                        principalTable: "SpecialOffers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "BuyOneProductAndGetDiscountOnOtherSpecialOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    RequiredCommoditiesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyOneProductAndGetDiscountOnOtherSpecialOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuyOneProductAndGetDiscountOnOtherSpecialOffers_SpecialOffers_Id",
                        column: x => x.Id,
                        principalTable: "SpecialOffers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "BuySetOfProductsToGetDiscountOnThemSpecialOffers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuySetOfProductsToGetDiscountOnThemSpecialOffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuySetOfProductsToGetDiscountOnThemSpecialOffers_SpecialOffers_Id",
                        column: x => x.Id,
                        principalTable: "SpecialOffers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CommodityOrderDM",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CommodityId = table.Column<int>(type: "int", nullable: false),
                    BasketId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityOrderDM", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommodityOrderDM_Baskets_BasketId",
                        column: x => x.BasketId,
                        principalTable: "Baskets",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CommodityOrderDM_Commodities_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commodities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommodityOrderRequirementDM",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CommodityId = table.Column<int>(type: "int", nullable: false),
                    AmountRequired = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CalculationLogic = table.Column<int>(type: "int", nullable: false),
                    BuySetOfProductsToGetDiscountOnThemSpecialOfferId = table.Column<int>(type: "int", nullable: true),
                    BuyOneProductAndGetDiscountOnOtherSpecialOfferId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityOrderRequirementDM", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommodityOrderRequirementDM_BuyOneProductAndGetDiscountOnOtherSpecialOffers_BuyOneProductAndGetDiscountOnOtherSpecialOfferId",
                        column: x => x.BuyOneProductAndGetDiscountOnOtherSpecialOfferId,
                        principalTable: "BuyOneProductAndGetDiscountOnOtherSpecialOffers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CommodityOrderRequirementDM_BuySetOfProductsToGetDiscountOnThemSpecialOffers_BuySetOfProductsToGetDiscountOnThemSpecialOffer~",
                        column: x => x.BuySetOfProductsToGetDiscountOnThemSpecialOfferId,
                        principalTable: "BuySetOfProductsToGetDiscountOnThemSpecialOffers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CommodityOrderRequirementDM_Commodities_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commodities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Quantities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UnitName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CommodityId = table.Column<int>(type: "int", nullable: true),
                    CommodityOrderId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quantities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quantities_Commodities_CommodityId",
                        column: x => x.CommodityId,
                        principalTable: "Commodities",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Quantities_CommodityOrderDM_CommodityOrderId",
                        column: x => x.CommodityOrderId,
                        principalTable: "CommodityOrderDM",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Commodities",
                columns: new[] { "Id", "Description", "Name", "PricePerUnit" },
                values: new object[,]
                {
                    { 1, "flavour: chicken", "Soup", 0.65m },
                    { 2, "calories: 251 kcal", "Bread", 0.80m },
                    { 3, "calories: 80 kcal", "Roll", 0.30m },
                    { 4, "fat: 3,2%", "Full-cream milk", 1.60m },
                    { 5, "fat: 1,5%", "Low-fat milk", 1.3m },
                    { 6, "origin: Poland", "Apples", 1.0m }
                });

            migrationBuilder.InsertData(
                table: "CustomerDM",
                columns: new[] { "Id", "BasketId", "Name" },
                values: new object[] { 1, 1, "Damian Dziedzic" });

            migrationBuilder.InsertData(
                table: "SpecialOffers",
                columns: new[] { "Id", "Description", "DiscountValue", "DiscountedAmount", "DiscountedCommodityId", "SpecialOfferType" },
                values: new object[,]
                {
                    { 1, "Apples have a 10% discount off their normal price this week", 0.1m, 9999m, 6, 0 },
                    { 2, "Buy 2 tins of soup and get one roll for half price", 0.5m, 1m, 3, 1 },
                    { 3, "Buy any milk and bread to get 20% discount on both", 0.2m, 1m, null, 2 }
                });

            migrationBuilder.InsertData(
                table: "ActiveDuringPeriodSpecialOffers",
                columns: new[] { "Id", "EndDate", "StartDate" },
                values: new object[] { 1, new DateTime(2022, 10, 22, 0, 0, 0, 0, DateTimeKind.Utc), new DateTime(2022, 9, 12, 0, 0, 0, 0, DateTimeKind.Utc) });

            migrationBuilder.InsertData(
                table: "Baskets",
                columns: new[] { "Id", "CustomerId", "TotalPrice" },
                values: new object[] { 1, 1, 0m });

            migrationBuilder.InsertData(
                table: "BuyOneProductAndGetDiscountOnOtherSpecialOffers",
                columns: new[] { "Id", "RequiredCommoditiesId" },
                values: new object[] { 2, 0 });

            migrationBuilder.InsertData(
                table: "BuySetOfProductsToGetDiscountOnThemSpecialOffers",
                column: "Id",
                value: 3);

            migrationBuilder.InsertData(
                table: "Quantities",
                columns: new[] { "Id", "CommodityId", "CommodityOrderId", "UnitName", "Value" },
                values: new object[,]
                {
                    { 1, 1, null, "tin", 10m },
                    { 2, 2, null, "loaf", 10m },
                    { 3, 3, null, "item", 10m },
                    { 4, 4, null, "bottle", 10m },
                    { 5, 5, null, "bottle", 10m },
                    { 6, 6, null, "bag", 10m }
                });

            migrationBuilder.InsertData(
                table: "CommodityOrderRequirementDM",
                columns: new[] { "Id", "AmountRequired", "BuyOneProductAndGetDiscountOnOtherSpecialOfferId", "BuySetOfProductsToGetDiscountOnThemSpecialOfferId", "CalculationLogic", "CommodityId" },
                values: new object[,]
                {
                    { 1, 2m, 2, null, 0, 1 },
                    { 2, 1m, null, 3, 0, 2 },
                    { 3, 1m, null, 3, 1, 4 },
                    { 4, 1m, null, 3, 1, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Baskets_CustomerId",
                table: "Baskets",
                column: "CustomerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CommodityOrderDM_BasketId",
                table: "CommodityOrderDM",
                column: "BasketId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityOrderDM_CommodityId",
                table: "CommodityOrderDM",
                column: "CommodityId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityOrderRequirementDM_BuyOneProductAndGetDiscountOnOtherSpecialOfferId",
                table: "CommodityOrderRequirementDM",
                column: "BuyOneProductAndGetDiscountOnOtherSpecialOfferId",
                unique: true,
                filter: "[BuyOneProductAndGetDiscountOnOtherSpecialOfferId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityOrderRequirementDM_BuySetOfProductsToGetDiscountOnThemSpecialOfferId",
                table: "CommodityOrderRequirementDM",
                column: "BuySetOfProductsToGetDiscountOnThemSpecialOfferId");

            migrationBuilder.CreateIndex(
                name: "IX_CommodityOrderRequirementDM_CommodityId",
                table: "CommodityOrderRequirementDM",
                column: "CommodityId");

            migrationBuilder.CreateIndex(
                name: "IX_Quantities_CommodityId",
                table: "Quantities",
                column: "CommodityId",
                unique: true,
                filter: "[CommodityId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Quantities_CommodityOrderId",
                table: "Quantities",
                column: "CommodityOrderId",
                unique: true,
                filter: "[CommodityOrderId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActiveDuringPeriodSpecialOffers");

            migrationBuilder.DropTable(
                name: "CommodityOrderRequirementDM");

            migrationBuilder.DropTable(
                name: "Quantities");

            migrationBuilder.DropTable(
                name: "BuyOneProductAndGetDiscountOnOtherSpecialOffers");

            migrationBuilder.DropTable(
                name: "BuySetOfProductsToGetDiscountOnThemSpecialOffers");

            migrationBuilder.DropTable(
                name: "CommodityOrderDM");

            migrationBuilder.DropTable(
                name: "SpecialOffers");

            migrationBuilder.DropTable(
                name: "Baskets");

            migrationBuilder.DropTable(
                name: "Commodities");

            migrationBuilder.DropTable(
                name: "CustomerDM");
        }
    }
}
