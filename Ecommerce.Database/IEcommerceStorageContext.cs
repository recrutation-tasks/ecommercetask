﻿using Ecommerce.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Database
{
    public interface IEcommerceStorageContext
    {
        DbSet<T> Get<T>() where T : Entity;
        void SaveChanges();
    }
}
