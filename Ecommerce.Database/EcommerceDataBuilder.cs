﻿using Ecommerce.Data.Model;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Database
{
    internal class EcommerceDataBuilder
    {
        private readonly ModelBuilder _builder;

        public EcommerceDataBuilder([NotNull] ModelBuilder builder)
            => _builder = builder ?? throw new ArgumentNullException(nameof(builder));

        internal void CreateInitialData()
        {
            int counter = 0;

            AddCustomer();

            CreateCommodity("Soup", 0.65m, "tin", "flavour: chicken", ref counter);
            CreateCommodity("Bread", 0.80m, "loaf", "calories: 251 kcal",  ref counter);
            CreateCommodity("Roll", 0.30m, "item", "calories: 80 kcal",  ref counter);
            CreateCommodity("Full-cream milk", 1.60m, "bottle", "fat: 3,2%",  ref counter);
            CreateCommodity("Low-fat milk", 1.3m, "bottle", "fat: 1,5%",  ref counter);
            CreateCommodity("Apples", 1.0m, "bag", "origin: Poland", ref counter);

            CreateAppleSpecialOffer(ref counter);
            CreateTinsOfSoupSpecialOffer(ref counter);
            CreateBreadAndMilkSpecialOffer(ref counter);
        }

        private void AddCustomer()
        {
            _builder.Entity<BasketDM>().HasData(new BasketDM
            {
                Id = 1,
                Commodities = new List<CommodityOrderDM>(),
                CustomerId = 1,
                TotalPrice = 0
            });

            _builder.Entity<CustomerDM>().HasData(new CustomerDM
            {
                Id = 1,
                Name = "Damian Dziedzic",
                BasketId = 1
            });
        }

        private void CreateCommodity([NotNull] string name, decimal price, [NotNull] string unitName, [CanBeNull] string description, ref int counter)
        {
            if (string.IsNullOrWhiteSpace(unitName))
                throw new ArgumentNullException(nameof(unitName));

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            counter++;

            _builder.Entity<QuantityDM>().HasData(new QuantityDM
            {
                UnitName = unitName,
                Id = counter,
                Value = 10,
                CommodityId = counter,
            });

            _builder.Entity<CommodityDM>().HasData(
                new CommodityDM
                {
                    Id = counter,
                    Name = name,
                    Description = description,
                    PricePerUnit = price,
                });
        }

        private void CreateAppleSpecialOffer(ref int counter)
        {
            _builder.Entity<ActiveDuringPeriodSpecialOfferDM>().HasData(new ActiveDuringPeriodSpecialOfferDM
            {
                Id = 1,
                Description = "Apples have a 10% discount off their normal price this week",
                StartDate = new DateTime(2022, 9, 12, 0, 0, 0, DateTimeKind.Utc),
                EndDate = new DateTime(2022, 10, 22, 0, 0, 0, DateTimeKind.Utc),
                DiscountedAmount = 9999,
                DiscountedCommodityId = 6,
                DiscountValue = 0.1m
            });
        }

        private void CreateTinsOfSoupSpecialOffer(ref int counter)
        {
            _builder.Entity<BuyOneProductAndGetDiscountOnOtherSpecialOfferDM>().HasData(new BuyOneProductAndGetDiscountOnOtherSpecialOfferDM
            {
                Id = 2,
                Description = "Buy 2 tins of soup and get one roll for half price",
                DiscountedAmount = 1,
                DiscountedCommodityId = 3,
                DiscountValue = 0.5m,
            });

            _builder.Entity<CommodityOrderRequirementDM>().HasData(new CommodityOrderRequirementDM
            {
                Id = 1,
                BuyOneProductAndGetDiscountOnOtherSpecialOfferId = 2,
                AmountRequired = 2,
                CalculationLogic = CalculationLogic.Required,
                CommodityId = 1,
            });
        }

        private void CreateBreadAndMilkSpecialOffer(ref int counter)
        {
            _builder.Entity<BuySetOfProductsToGetDiscountOnThemSpecialOfferDM>().HasData(new BuySetOfProductsToGetDiscountOnThemSpecialOfferDM
            {
                Id = 3,
                Description = "Buy any milk and bread to get 20% discount on both",
                DiscountedAmount = 1,
                DiscountedCommodityId = null,
                DiscountValue = 0.2m,
            });

            _builder.Entity<CommodityOrderRequirementDM>().HasData(new CommodityOrderRequirementDM
            {
                Id = 2,
                BuySetOfProductsToGetDiscountOnThemSpecialOfferId = 3,
                AmountRequired = 1,
                CalculationLogic = CalculationLogic.Required,
                CommodityId = 2,
            });

            _builder.Entity<CommodityOrderRequirementDM>().HasData(new CommodityOrderRequirementDM
            {
                Id = 3,
                BuySetOfProductsToGetDiscountOnThemSpecialOfferId = 3,
                AmountRequired = 1,
                CalculationLogic = CalculationLogic.Any,
                CommodityId = 4,
            });

            _builder.Entity<CommodityOrderRequirementDM>().HasData(new CommodityOrderRequirementDM
            {
                Id = 4,
                BuySetOfProductsToGetDiscountOnThemSpecialOfferId = 3,
                AmountRequired = 1,
                CalculationLogic = CalculationLogic.Any,
                CommodityId = 5,
            });
        }
    }
}
