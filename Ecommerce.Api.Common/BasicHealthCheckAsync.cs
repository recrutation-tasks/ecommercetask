﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Ecommerce.Api.Common
{
    public class BasicHealthCheckAsync : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(
            HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                return Task.FromResult(
                    HealthCheckResult.Healthy());
            }
            catch (Exception)
            {
                return Task.FromResult(
                    new HealthCheckResult(
                        context.Registration.FailureStatus));
            }
        }
    }
}
