﻿namespace Ecommerce.DataTransfer.Model
{
    public class CommodityBasicInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal PricePerUnit { get; set; }
        public string UnitName { get; set; }
        public string CurrencySymbol { get; set; }
    }
}
