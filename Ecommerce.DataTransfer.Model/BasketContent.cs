﻿using System.Collections.Generic;

namespace Ecommerce.DataTransfer.Model
{
    public class BasketContent
    {
        public int CustomerId { get; set; }
        public decimal TotalPrice { get; set; }
        public IReadOnlyCollection<CommodityOrder> OrderedCommodities { get; set; }
    }
}
