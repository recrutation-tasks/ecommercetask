﻿namespace Ecommerce.DataTransfer.Model
{
    public class DiscountBasicInfo
    {
        public int CommodityId { get; set; }
        public decimal Percentage { get; set; }
        public decimal Amount { get; set; }
    }
}
