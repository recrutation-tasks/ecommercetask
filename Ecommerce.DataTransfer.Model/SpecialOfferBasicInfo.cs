﻿namespace Ecommerce.DataTransfer.Model
{
    public class SpecialOfferBasicInfo
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
