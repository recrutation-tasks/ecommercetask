﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.DataTransfer.Model
{
    public class CommodityOrder
    {
        public int CommodityId { get; set; }
        public string CommodityName { get; set; }
        public decimal CommodityTotalPrice { get; set; }
        public decimal CommodityQuantity { get; set; }
        public string QuantityUnit { get; set; }
    }
}
