﻿using System;

namespace Ecommerce.DataTransfer.Model
{
    public class CommodityOrderBasicInfo : ICloneable
    {
        public int CommodityId { get; set; }
        public decimal Quantity { get; set; }

        public object Clone() => MemberwiseClone();
    }
}
