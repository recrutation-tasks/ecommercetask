using Ecommerce.Domain.Model;
using JetBrains.Annotations;
using System;

/// <summary>
/// Aggregate root for StoreContext, describes detailed information about commodity offered by store
/// </summary>
public class Commodity : DomainEntity
{
    /// <summary>
    /// Creates new instance of Commodity class
    /// </summary>
    /// <param name="id">Unique identifier</param>
    /// <param name="name">Commodity name</param>
    /// <param name="decription">Commodity description</param>
    /// <param name="pricePerUnit">Price per one unit of commodity in USD</param>
    /// <param name="quantity">Total amount of available goods</param>
    /// <exception cref="ArgumentNullException">Occures when any of fallowing arguments is null: name, pricePerUnit, quantity</exception>
    public Commodity(int id,
        [NotNull] string name,
        [CanBeNull] string decription,
        [NotNull] Quantity quantity,
        decimal pricePerUnit) : base(id)
    {
        Name = name ?? throw new ArgumentNullException();
        Quantity = quantity ?? throw new ArgumentNullException();
        Description = decription;
        PricePerUnit = pricePerUnit;
    }
    /// <summary>
    /// Commodity name
    /// </summary>
    public string Name { get; }
    /// <summary>
    /// Commodity description
    /// </summary>
    public string Description { get; }
    /// <summary>
    /// Price per one unit of commodity in USD
    /// </summary>
    public decimal PricePerUnit { get; }
    /// <summary>
    /// Total amount of available goods
    /// </summary>
    public Quantity Quantity { get; }
} 