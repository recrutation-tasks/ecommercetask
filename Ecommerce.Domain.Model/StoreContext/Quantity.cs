using Ecommerce.Domain.Model;
using JetBrains.Annotations;

/// <summary>
/// Describes quantity of goods
/// </summary>
public class Quantity : DomainEntity
{

    /// <summary>
    /// Creates new instance of Quantity class
    /// </summary>
    /// <param name="id">Unique identifier</param>
    /// <param name="value">Amount of goods</param>
    /// <param name="unitName">Unit of measure</param>
    public Quantity(int id,
        decimal value, 
        [NotNull] string unitName) : base(id)
    {
        Value = value;
        UnitName = unitName;
    }

    /// <summary>
    /// Amount of goods
    /// </summary>
    public decimal Value { get; private set; }
    /// <summary>
    /// Unit of measure
    /// </summary>
    [NotNull]
    public string UnitName { get; private set; }
}