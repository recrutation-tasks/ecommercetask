﻿using JetBrains.Annotations;
using System;

namespace Ecommerce.Domain.Model.BasketContext
{
    /// <summary>
    /// Describes relation between commodities and customer order
    /// </summary>
    public class CommodityOrder : DomainEntity
    {
        /// <summary>
        /// Creates new instance of Commodity order class
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="commodityId">Ordered commodity unique identifier</param>
        /// <param name="quantity">Amout of commodity that has been orderd</param>
        /// <param name="commodityName">Name of commodity</param>
        /// <param name="quantityUnit">Unit of measure describing quantity that has been ordered</param>
        /// <param name="commodityPrice">Price of commodity per unit</param>
        /// <exception cref="ArgumentNullException"></exception>
        public CommodityOrder(int id,
            int commodityId, 
            decimal quantity,
            [NotNull] string commodityName, 
            [NotNull] string quantityUnit, 
            decimal commodityPrice) : base(id)
        {
            if (string.IsNullOrWhiteSpace(commodityName))
                throw new ArgumentNullException(nameof(commodityName));

            if (string.IsNullOrWhiteSpace(quantityUnit))
                throw new ArgumentNullException(nameof(quantityUnit));

            CommodityId = commodityId;
            CommodityName = commodityName;
            CommodityQuantity = quantity;
            QuantityUnit = quantityUnit;
            CommodityPrice = commodityPrice;
        }

        /// <summary>
        /// Ordered commodity unique identifier
        /// </summary>
        public int CommodityId { get; }
        /// <summary>
        /// Amout of commodity that has been orderd
        /// </summary>
        public string CommodityName { get; }
        /// <summary>
        /// Total price of commodity in USD, including total quantity ordered and discounts
        /// </summary>
        public decimal? CommodityTotalPrice { get; private set; }
        /// <summary>
        /// Amout of commodity that has been orderd
        /// </summary>
        public decimal CommodityQuantity { get; }
        /// <summary>
        /// Unit of measure describing quantity that has been ordered
        /// </summary>
        public string QuantityUnit { get; }
        /// <summary>
        /// Price of commodity per unit
        /// </summary>
        public decimal CommodityPrice { get; }
        /// <summary>
        /// Sets value for CommodityTotalPrice
        /// </summary>
        /// <param name="Price">Price of commodity includint quantity and discounts</param>
        public void SetPrice(decimal Price) => CommodityTotalPrice = Price;
    }
}
