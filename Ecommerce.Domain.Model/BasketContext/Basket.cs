﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;

namespace Ecommerce.Domain.Model.BasketContext
{
    /// <summary>
    /// Aggregate root for Basket context
    /// </summary>
    public class Basket : DomainEntity
    {
        /// <summary>
        /// Creates new instance of Basket class
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="commodities">Ordered commodities</param>
        /// <param name="totalPrice">Total price of ordered commodities in USD</param>
        /// <exception cref="ArgumentNullException"></exception>
        public Basket(int id,
            int customerId,
            [NotNull] IReadOnlyCollection<CommodityOrder> commodities) : base(id)
        {
            OrderedCommodities = commodities ?? throw new ArgumentNullException(nameof(commodities));
            CustomerId = customerId;
        }

        /// <summary>
        /// Id of customer that owns the basket
        /// </summary>
        public int CustomerId { get; private set; }
        /// <summary>
        /// Ordered commodities
        /// </summary>
        [NotNull]
        public IReadOnlyCollection<CommodityOrder> OrderedCommodities { get; private set; }
        /// <summary>
        /// Total price of ordered commodities in USD
        /// </summary>
        [NotNull]
        public decimal TotalPrice { get; private set; }
        /// <summary>
        /// Total price including discounts
        /// </summary>
        /// <param name="price">Price in USD</param>

        public void SetPrice(decimal price) => TotalPrice = price;
    }
}
