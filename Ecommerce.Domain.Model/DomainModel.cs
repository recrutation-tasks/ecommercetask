﻿namespace Ecommerce.Domain.Model
{
    /// <summary>
    /// Base class for domain entities
    /// </summary>
    public abstract class DomainEntity
    {
        /// <summary>
        /// Ensures that each domain entity has unique identifier
        /// </summary>
        /// <param name="id"></param>
        protected DomainEntity(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Unique entity identifier
        /// </summary>
        public int Id { get; private set; }
    }
}
