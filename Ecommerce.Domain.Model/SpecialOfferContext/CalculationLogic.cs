﻿namespace Ecommerce.Domain.Model.SpecialOfferContext
{
    /// <summary>
    /// Describes calculation method for discounts
    /// </summary>
    public enum CalculationLogic
    {
        /// <summary>
        /// All commodities marked as required have to be ordred for discount to be applied
        /// </summary>
        Required,
        /// <summary>
        /// Only one commodity marked as any have to be ordred for discount to be applied
        /// </summary>
        Any
    }
}
