using JetBrains.Annotations;

namespace Ecommerce.Domain.Model.SpecialOfferContext
{
    /// <summary>
    /// Aggregate root for Special offer context, describes details of special offer
    /// </summary>
    public abstract class SpecialOffer : DomainEntity
    {
        /// <summary>
        /// Ensures that specific offer classes will include basic information
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="description">Special offer description</param>
        /// <param name="discountValue">Discount value percentage 0-1 of total price</param>
        /// <param name="discountedCommodityId">Identifier of commodity to be discounted</param>
        /// <param name="discountedAmount">Amount of commodity that discount applies to</param>
        protected SpecialOffer(int id,
            [CanBeNull] string description,
            decimal discountValue,
            int? discountedCommodityId,
            decimal discountedAmount) : base(id)
        {
            Description = description;
            DiscountValue = discountValue;
            DiscountedCommodityId = discountedCommodityId;
            DiscountedAmount = discountedAmount;
        }

        /// <summary>
        /// Special offer description
        /// </summary>
        public string Description { get; private set; }
        /// <summary>
        /// Requirements that have to be met in order for discount to take place
        /// </summary>
        public SpecialOfferType SpecialOfferType { get; protected set; }
        /// <summary>
        /// Discount value percentage 0-1 of total price
        /// </summary>
        public decimal DiscountValue { get; private set; }
        /// <summary>
        /// Identifier of commodity to be discounted
        /// </summary>
        public int? DiscountedCommodityId { get; private set; }
        /// <summary>
        /// Amount of commodity that discount applies to
        /// </summary>
        public decimal DiscountedAmount { get; private set; }
    }
}

