﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;

namespace Ecommerce.Domain.Model.SpecialOfferContext
{
    /// <summary>
    /// Special offer that is active after customer orderes specific set of products
    /// </summary>
    public class BuySetOfProductsToGetDiscountOnThemSpecialOffer : SpecialOffer
    {
        /// <summary>
        /// Creates new instance of BuySetOfProductsToGetDiscountOnThemSpecialOffer
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="description">Special offer description</param>
        /// <param name="discountValue">Discount value percentage 0-1 of total price</param>
        /// <param name="discountedCommodityId">Identifier of commodity to be discounted</param>
        /// <param name="discountedAmount">Amount of commodity that discount applies to</param>
        /// <param name="requiredCommodities">Commodities that has to be ordered</param>
        /// <exception cref="ArgumentNullException">Occures when requiredCommodities argument is null</exception>
        public BuySetOfProductsToGetDiscountOnThemSpecialOffer(int id,
            [CanBeNull] string description, 
            decimal discountValue, 
            int? discountedCommodityId, 
            decimal discountedAmount,
            [NotNull] IReadOnlyCollection<CommodityOrderRequirement> requiredCommodities) : base(id, description, discountValue, discountedCommodityId, discountedAmount)
        {
            RequiredCommodities = requiredCommodities ?? throw new ArgumentNullException();
            SpecialOfferType = SpecialOfferType.BuySetOfProductsToGetDiscountOnThem;
        }
        /// <summary>
        /// Commodities that has to be ordered
        /// </summary>
        [NotNull]
        public IReadOnlyCollection<CommodityOrderRequirement> RequiredCommodities { get; private set; }
    }
}
