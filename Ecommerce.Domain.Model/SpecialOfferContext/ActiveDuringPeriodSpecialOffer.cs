﻿using JetBrains.Annotations;
using System;

namespace Ecommerce.Domain.Model.SpecialOfferContext
{
    /// <summary>
    /// Special offer that is active during certain period of time
    /// </summary>
    public class ActiveDuringPeriodSpecialOffer : SpecialOffer
    {
        /// <summary>
        /// Creates new instance of ActiveDuringPeriodSpecialOffer
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="description">Special offer description</param>
        /// <param name="discountValue">Discount value percentage 0-1 of total price</param>
        /// <param name="discountedCommodityId">Identifier of commodity to be discounted</param>
        /// <param name="discountAmount">Amount of commodity that discount applies to</param>
        /// <param name="startDate">Special offer starting date</param>
        /// <param name="endDate">Special offer end date</param>
        public ActiveDuringPeriodSpecialOffer(int id,
            string description, 
            decimal discountValue, 
            int? discountedCommodityId, 
            decimal discountAmount,
            [CanBeNull] DateTime? startDate,
            [CanBeNull] DateTime? endDate) : base(id, description, discountValue, discountedCommodityId, discountAmount)
        {
            StartDate = startDate;
            EndDate = endDate;
            SpecialOfferType = SpecialOfferType.ActiveDuringPeriod;
        }

        /// <summary>
        /// Related special offer starting date ( UTC )
        /// </summary>
        [CanBeNull]
        public DateTime? StartDate { get; private set; }
        /// <summary>
        /// Related special offer end date ( UTC )
        /// </summary>
        [CanBeNull]
        public DateTime? EndDate { get; private set; }
    }
}
