﻿using JetBrains.Annotations;
using System;

namespace Ecommerce.Domain.Model.SpecialOfferContext
{
    /// <summary>
    /// Special offer that is active after customer orders certain amount of specific commodity
    /// </summary>
    public class BuyOneProductAndGetDiscountOnOtherSpecialOffer : SpecialOffer
    {
        /// <summary>
        /// Creates new instance of BuyOneProductAndGetDiscountOnOtherSpecialOffer
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="description">Special offer description</param>
        /// <param name="discountValue">Discount value percentage 0-1 of total price</param>
        /// <param name="discountedCommodityId">Identifier of commodity to be discounted</param>
        /// <param name="discountAmount">Amount of commodity that discount applies to</param>
        /// <param name="requiredCommodities">Commodities that has to be ordered</param>
        /// <exception cref="ArgumentNullException">Occures when requiredCommodities argument is null</exception>
        public BuyOneProductAndGetDiscountOnOtherSpecialOffer(int id,
            [CanBeNull] string description, 
            decimal discountValue, 
            int? discountedCommodityId, 
            decimal discountAmount,
            [NotNull] CommodityOrderRequirement requiredCommodities) : base(id, description, discountValue, discountedCommodityId, discountAmount)
        {
            RequiredCommodities = requiredCommodities ?? throw new ArgumentNullException();
            SpecialOfferType = SpecialOfferType.BuyOneProductAndGetDiscountOnOther;
        }
        /// <summary>
        /// Commodities that has to be ordered
        /// </summary>
        [NotNull]
        public CommodityOrderRequirement RequiredCommodities { get; private set; }
    }
}
