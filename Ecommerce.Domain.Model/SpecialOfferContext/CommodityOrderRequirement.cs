﻿namespace Ecommerce.Domain.Model.SpecialOfferContext
{
    /// <summary>
    /// Describes commodity requirement in context of discounts
    /// </summary>
    public class CommodityOrderRequirement : DomainEntity
    {
        /// <summary>
        /// Creates new instance of CommodityOrderRequirement
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <param name="commodityId">Commodity unique identifier</param>
        /// <param name="amount">Amount that has to be ordered</param>
        /// <param name="calculationLogic">Required or any of commodities</param>
        public CommodityOrderRequirement(int id, 
            int commodityId, 
            decimal amount, 
            CalculationLogic calculationLogic) : base (id)
        {
            CommodityId = commodityId;
            AmountRequired = amount;
            CalculationLogic = calculationLogic;
        }

        /// <summary>
        /// Commodity unique identifier
        /// </summary>
        public int CommodityId { get; }
        /// <summary>
        /// Amount that has to be ordere
        /// </summary>
        public decimal AmountRequired { get; }
        /// <summary>
        /// Required or any of commodities
        /// </summary>
        public CalculationLogic CalculationLogic { get; }
    }
}
