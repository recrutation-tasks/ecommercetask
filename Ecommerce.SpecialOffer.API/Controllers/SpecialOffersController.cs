﻿using Ecommerce.SpecialOffer.API.Domain.Queries;
using JetBrains.Annotations;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.SpecialOffer.API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class SpecialOffersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SpecialOffersController([NotNull] IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }


        [Route("BasicInfo")]
        [HttpGet]
        public async Task<IActionResult> BasicInfo(CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetSpecialOfferBasicInfoQuery(), cancellationToken));
        }
    }
}
