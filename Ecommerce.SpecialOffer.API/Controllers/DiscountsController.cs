﻿using Ecommerce.DataTransfer.Model;
using Ecommerce.SpecialOffer.API.Domain.Queries;
using JetBrains.Annotations;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.SpecialOffer.API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DiscountsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DiscountsController([NotNull] IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }


        [Route("Applied")]
        [HttpPost]
        public async Task<IActionResult> Discounts([FromBody] IReadOnlyCollection<CommodityOrderBasicInfo> orders, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(new GetActiveDiscountsQuery(orders), cancellationToken));
        }
    }
}
