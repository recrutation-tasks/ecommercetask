﻿using Ecommerce.DataTransfer.Model;
using JetBrains.Annotations;
using MediatR;

namespace Ecommerce.SpecialOffer.API.Domain.Queries
{
    public class GetActiveDiscountsQuery : IRequest<IReadOnlyCollection<DiscountBasicInfo>>   
    {

        public GetActiveDiscountsQuery([NotNull] IReadOnlyCollection<CommodityOrderBasicInfo> commodityOrders)
        {
            CommodityOrders = commodityOrders ?? throw new ArgumentNullException(nameof(commodityOrders));
        }

        public IReadOnlyCollection<CommodityOrderBasicInfo> CommodityOrders { get; }
    }
}
