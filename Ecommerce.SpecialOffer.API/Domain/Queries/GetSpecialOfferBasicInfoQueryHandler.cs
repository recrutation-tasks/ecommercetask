﻿using AutoMapper;
using Ecommerce.DataTransfer.Model;
using Ecommerce.SpecialOffer.API.Domain.Contexts;
using JetBrains.Annotations;
using MediatR;

namespace Ecommerce.SpecialOffer.API.Domain.Queries
{
    public class GetSpecialOfferBasicInfoQueryHandler : IRequestHandler<GetSpecialOfferBasicInfoQuery, IReadOnlyCollection<SpecialOfferBasicInfo>>
    {
        private readonly IDomainContext _domainContext;
        private readonly IMapper _mapper;

        public GetSpecialOfferBasicInfoQueryHandler([NotNull] IDomainContext domainContext, [NotNull] IMapper mapper)
        {
            _domainContext = domainContext ?? throw new ArgumentNullException(nameof(domainContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<IReadOnlyCollection<SpecialOfferBasicInfo>> Handle([NotNull] GetSpecialOfferBasicInfoQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            if (cancellationToken.IsCancellationRequested)
                throw new OperationCanceledException(cancellationToken);

            var specialOffers = _domainContext.GetAll();

            return Task.FromResult(_mapper.Map<IEnumerable<Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer>, IEnumerable<SpecialOfferBasicInfo>>(specialOffers).ToList() as IReadOnlyCollection<SpecialOfferBasicInfo>);
        }
    }
}
