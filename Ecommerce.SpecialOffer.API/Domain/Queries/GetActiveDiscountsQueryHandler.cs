﻿using Ecommerce.DataTransfer.Model;
using Ecommerce.Domain.Model.SpecialOfferContext;
using Ecommerce.SpecialOffer.API.Domain.Contexts;
using Ecommerce.SpecialOffer.API.Domain.Services;
using JetBrains.Annotations;
using MediatR;

namespace Ecommerce.SpecialOffer.API.Domain.Queries
{
    public class GetActiveDiscountsQueryHandler : IRequestHandler<GetActiveDiscountsQuery, IReadOnlyCollection<DiscountBasicInfo>>
    {
        private readonly IDomainContext _domainContext;
        private readonly IDiscountSelectorFactory _discountSelectorFactory;
        
        public GetActiveDiscountsQueryHandler([NotNull] IDomainContext domainContext, [NotNull] IDiscountSelectorFactory discountSelectorFactory)
        {
            _domainContext = domainContext ?? throw new ArgumentNullException(nameof(domainContext));
            _discountSelectorFactory = discountSelectorFactory ?? throw new ArgumentNullException(nameof(discountSelectorFactory));
        }

        public Task<IReadOnlyCollection<DiscountBasicInfo>> Handle([NotNull] GetActiveDiscountsQuery request, CancellationToken cancellationToken)
        {
            if (request is null)
                throw new ArgumentNullException(nameof(request));

            if (cancellationToken.IsCancellationRequested)
                throw new OperationCanceledException();

            var result = new List<DiscountBasicInfo>();
            var specialOffers = _domainContext.GetAll();

            foreach(var sp in specialOffers)
            {
                var discountSelector = _discountSelectorFactory.Create(sp.SpecialOfferType);
                result.AddRange(discountSelector.Get(sp, request.CommodityOrders));
            }

            return Task.FromResult(result as IReadOnlyCollection<DiscountBasicInfo>);
        }
    }
}
