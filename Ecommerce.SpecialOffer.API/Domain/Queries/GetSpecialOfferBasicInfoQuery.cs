﻿using Ecommerce.DataTransfer.Model;
using MediatR;

namespace Ecommerce.SpecialOffer.API.Domain.Queries
{
    public class GetSpecialOfferBasicInfoQuery : IRequest<IReadOnlyCollection<SpecialOfferBasicInfo>>
    {
    }
}
