﻿using Ecommerce.DataTransfer.Model;

namespace Ecommerce.SpecialOffer.API.Domain.Contexts
{
    public interface IDomainContext
    {
        IReadOnlyCollection<Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer> GetAll();

    }
}
