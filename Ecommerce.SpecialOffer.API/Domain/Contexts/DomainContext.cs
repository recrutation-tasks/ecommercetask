﻿using AutoMapper;
using Ecommerce.Data.Model;
using Ecommerce.Database;
using Ecommerce.Domain.Model.SpecialOfferContext;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.SpecialOffer.API.Domain.Contexts
{
    public class DomainContext : IDomainContext
    {
        private readonly IEcommerceStorageContext _storageContext;
        private readonly IMapper _mapper;

        public DomainContext([NotNull] IEcommerceStorageContext storageContext, [NotNull] IMapper mapper)
        {
            _storageContext = storageContext ?? throw new ArgumentNullException(nameof(storageContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));    
        }

        public IReadOnlyCollection<Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer> GetAll()
        {
            var result = new List<Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer>();

            var periodicOffers = _storageContext.Get<ActiveDuringPeriodSpecialOfferDM>().ToList();
            result.AddRange(periodicOffers.Select(x => new ActiveDuringPeriodSpecialOffer(x.Id, x.Description, x.DiscountValue, x.DiscountedCommodityId, x.DiscountedAmount, x.StartDate, x.EndDate)));
            var bopOffers = _storageContext.Get<BuyOneProductAndGetDiscountOnOtherSpecialOfferDM>().Include(x => x.RequiredCommodities).ToList();
            
            result.AddRange(
                bopOffers.Select(x => new BuyOneProductAndGetDiscountOnOtherSpecialOffer(x.Id, x.Description, x.DiscountValue, x.DiscountedCommodityId, x.DiscountedAmount,
                new CommodityOrderRequirement(x.RequiredCommodities.Id,
                    x.RequiredCommodities.CommodityId, 
                    x.RequiredCommodities.AmountRequired, 
                    _mapper.Map<Ecommerce.Data.Model.CalculationLogic, Ecommerce.Domain.Model.SpecialOfferContext.CalculationLogic>(x.RequiredCommodities.CalculationLogic)))));

            var bsOffers = _storageContext.Get<BuySetOfProductsToGetDiscountOnThemSpecialOfferDM>().Include(x => x.RequiredCommodities).ToList();

            result.AddRange(
                bsOffers.Select(x => new BuySetOfProductsToGetDiscountOnThemSpecialOffer(x.Id, x.Description, x.DiscountValue, x.DiscountedCommodityId, x.DiscountedAmount,
                x.RequiredCommodities.Select(rc =>
                new CommodityOrderRequirement(rc.Id, rc.CommodityId, rc.AmountRequired,
                _mapper.Map<Ecommerce.Data.Model.CalculationLogic, Ecommerce.Domain.Model.SpecialOfferContext.CalculationLogic>(rc.CalculationLogic))).ToList())));

            return result.ToList();
         }
    }
}
