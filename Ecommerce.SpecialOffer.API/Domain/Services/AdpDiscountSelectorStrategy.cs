﻿using Ecommerce.DataTransfer.Model;
using Ecommerce.Domain.Model.SpecialOfferContext;
using JetBrains.Annotations;

namespace Ecommerce.SpecialOffer.API.Domain.Services
{
    public class AdpDiscountSelectorStrategy : IApplyDiscountsStrategy
    {
        public IReadOnlyCollection<DiscountBasicInfo> Get([NotNull] Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer offer,[CanBeNull] IReadOnlyCollection<CommodityOrderBasicInfo> currentOrders)
        {
            if (offer is null)
                throw new ArgumentNullException(nameof(offer));

            var adpOffer = offer as ActiveDuringPeriodSpecialOffer;
            if (adpOffer is null)
                return Enumerable.Empty<DiscountBasicInfo>().ToList();

            if ((!adpOffer.StartDate.HasValue || adpOffer.StartDate.Value <= DateTime.UtcNow)
                && (!adpOffer.EndDate.HasValue || adpOffer.EndDate >= DateTime.UtcNow)
                && currentOrders.Any(x => x.CommodityId == adpOffer.DiscountedCommodityId))
                return new List<DiscountBasicInfo> { new DiscountBasicInfo { CommodityId = adpOffer.DiscountedCommodityId.Value, Amount = 9999, Percentage = adpOffer.DiscountValue } };

            return Enumerable.Empty<DiscountBasicInfo>().ToList();
        }
    }
}


