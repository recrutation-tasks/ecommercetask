﻿using Ecommerce.DataTransfer.Model;

namespace Ecommerce.SpecialOffer.API.Domain.Services
{
    public interface IApplyDiscountsStrategy
    {
        IReadOnlyCollection<DiscountBasicInfo> Get(Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer offer, IReadOnlyCollection<CommodityOrderBasicInfo> currentOrders);
    }
}
