﻿using Ecommerce.Domain.Model.SpecialOfferContext;

namespace Ecommerce.SpecialOffer.API.Domain.Services
{
    public interface IDiscountSelectorFactory
    {
        IApplyDiscountsStrategy Create(SpecialOfferType type);
    }
}
