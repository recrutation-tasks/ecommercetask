﻿using Ecommerce.Domain.Model.SpecialOfferContext;

namespace Ecommerce.SpecialOffer.API.Domain.Services
{
    public class DiscountSelectorFactory : IDiscountSelectorFactory
    {
        public IApplyDiscountsStrategy Create(SpecialOfferType type) => type switch
        {
            SpecialOfferType.ActiveDuringPeriod => new AdpDiscountSelectorStrategy(),
            SpecialOfferType.BuySetOfProductsToGetDiscountOnThem => new BuySetOfProdDiscountSelectorStrategy(),
            SpecialOfferType.BuyOneProductAndGetDiscountOnOther => new BuyOneProdDiscountSelectorStrategy(),
            _ => throw new NotSupportedException($"Special offer type {type} is not supported")
        };
    }
}
