﻿using Ecommerce.DataTransfer.Model;
using Ecommerce.Domain.Model.SpecialOfferContext;
using JetBrains.Annotations;

namespace Ecommerce.SpecialOffer.API.Domain.Services
{
    public class BuyOneProdDiscountSelectorStrategy : IApplyDiscountsStrategy
    {
        public IReadOnlyCollection<DiscountBasicInfo> Get([NotNull] Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer offer,[NotNull] IReadOnlyCollection<CommodityOrderBasicInfo> currentOrders)
        {
            if (offer is null)
                throw new ArgumentNullException(nameof(offer));

            if (currentOrders is null)
                throw new ArgumentNullException(nameof(currentOrders));

            var buyOneOffer = offer as BuyOneProductAndGetDiscountOnOtherSpecialOffer;
            if (buyOneOffer is null)
                return Enumerable.Empty<DiscountBasicInfo>().ToList();

            var orderedProducts = currentOrders.FirstOrDefault(x => x.CommodityId == buyOneOffer.RequiredCommodities.CommodityId);
            if (orderedProducts is null)
                return Enumerable.Empty<DiscountBasicInfo>().ToList();

            var result = new List<DiscountBasicInfo>();

            
            var amountApplied = Math.Round(orderedProducts.Quantity / buyOneOffer.RequiredCommodities.AmountRequired,0, MidpointRounding.ToNegativeInfinity);
            if (amountApplied == 0)
                return Enumerable.Empty<DiscountBasicInfo>().ToList();

            var discount = new DiscountBasicInfo { CommodityId = buyOneOffer.DiscountedCommodityId.Value, Percentage = buyOneOffer.DiscountValue, Amount = 0 };

            for (int i = 0; i < amountApplied; i++)
                discount.Amount += buyOneOffer.DiscountedAmount;

            result.Add(discount);
            return result;
        }
    }
}
