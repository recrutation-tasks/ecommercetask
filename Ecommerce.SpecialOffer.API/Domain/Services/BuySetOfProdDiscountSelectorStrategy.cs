﻿using Ecommerce.DataTransfer.Model;
using Ecommerce.Domain.Model.SpecialOfferContext;
using JetBrains.Annotations;

namespace Ecommerce.SpecialOffer.API.Domain.Services
{
    public class BuySetOfProdDiscountSelectorStrategy : IApplyDiscountsStrategy
    {
        public IReadOnlyCollection<DiscountBasicInfo> Get([NotNull] Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer offer,[NotNull] IReadOnlyCollection<CommodityOrderBasicInfo> currentOrders)
        {
            if (offer is null)
                throw new ArgumentNullException(nameof(offer));

            if (currentOrders is null)
                throw new ArgumentNullException(nameof(currentOrders));

            var bsoOffer = offer as BuySetOfProductsToGetDiscountOnThemSpecialOffer;
            if (bsoOffer is null)
                return Enumerable.Empty<DiscountBasicInfo>().ToList();

            var currentOrdersClone = currentOrders.Select(x => (CommodityOrderBasicInfo)x.Clone()).ToList();
            var discountParts = new List<DiscountBasicInfo>();

            while (true)
            {
                var required = MatchRequired(bsoOffer, currentOrdersClone);
                if (!required.Any())
                    break;

                var optional = FindAnyOptional(bsoOffer, currentOrdersClone);
                if (optional is null)
                    break;
                discountParts.AddRange(required);
                discountParts.Add(optional);
            }

            var ids = discountParts.Select(x => x.CommodityId).Distinct();
            var result = new List<DiscountBasicInfo>();

            foreach(var id in ids) {
                result.Add(new DiscountBasicInfo
                {
                    CommodityId = id,
                    Percentage = discountParts.First(dp => dp.CommodityId == id).Percentage,
                    Amount = discountParts.Where(dp => dp.CommodityId == id).Select(dp => dp.Amount).Sum()
                });
            }

            return result;
        }

        private IReadOnlyCollection<DiscountBasicInfo> MatchRequired([NotNull] BuySetOfProductsToGetDiscountOnThemSpecialOffer offer
            , [NotNull] IEnumerable<CommodityOrderBasicInfo> orders)
        {
            if (offer is null) throw new ArgumentNullException(nameof(offer));
            if (orders is null) throw new ArgumentNullException(nameof(orders));

            var result = new List<DiscountBasicInfo>();

            foreach (var commodity in offer.RequiredCommodities.Where(x => x.CalculationLogic == CalculationLogic.Required))
            {
                var found = orders.FirstOrDefault(cd => cd.CommodityId == commodity.CommodityId);
                if (found is null || found.Quantity < commodity.AmountRequired)
                    return Enumerable.Empty<DiscountBasicInfo>().ToList();

                result.Add(new DiscountBasicInfo { CommodityId = commodity.CommodityId, Percentage = offer.DiscountValue, Amount = offer.DiscountedAmount });
                found.Quantity = found.Quantity - offer.DiscountedAmount;
            }

            return result;
        }

        private DiscountBasicInfo? FindAnyOptional([NotNull] BuySetOfProductsToGetDiscountOnThemSpecialOffer offer
            , [NotNull] IEnumerable<CommodityOrderBasicInfo> orders)
        {
            if (offer is null) throw new ArgumentNullException(nameof(offer));
            if (orders is null) throw new ArgumentNullException(nameof(orders));

            var discountedOptional = orders.FirstOrDefault(x => offer.RequiredCommodities
                                           .Where(bso => bso.CalculationLogic == CalculationLogic.Any).Select(bso => bso.CommodityId).Contains(x.CommodityId)
                                                         && x.Quantity >= offer.RequiredCommodities.First(bso => bso.CommodityId == x.CommodityId).AmountRequired);

            if (discountedOptional is null)
            {
                return null;
            } else
            {
                discountedOptional.Quantity = discountedOptional.Quantity - offer.DiscountedAmount;
                return new DiscountBasicInfo { CommodityId = discountedOptional.CommodityId, Percentage = offer.DiscountValue, Amount = offer.DiscountedAmount };
            }
        }
    }
}
