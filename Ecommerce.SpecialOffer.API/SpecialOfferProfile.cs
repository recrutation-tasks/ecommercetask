﻿using AutoMapper;
using Ecommerce.Data.Model;
using Ecommerce.DataTransfer.Model;

namespace Ecommerce.SpecialOffer.API
{
    public class SpecialOfferProfile : Profile
    {
        public SpecialOfferProfile()
        {
            CreateMap<Ecommerce.Domain.Model.SpecialOfferContext.SpecialOffer, SpecialOfferBasicInfo>();
            CreateMap<SpecialOfferType, Ecommerce.Domain.Model.SpecialOfferContext.SpecialOfferType>();
        }
    }
}
